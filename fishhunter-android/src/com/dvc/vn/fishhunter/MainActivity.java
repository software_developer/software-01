package com.dvc.vn.fishhunter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.widget.Toast;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.dvc.vn.fishhunter.Assets;
import com.dvc.vn.fishhunter.IContact;
import com.dvc.vn.fishhunter.MainGame;
import com.dvc.vn.fishhunter.Settings;
import com.dvc.vn.fishhunter.ultility.SMSProcess;

public class MainActivity extends AndroidApplication implements IContact {
    MainGame game;
    PendingIntent sentPI;
    String SENT = "SMS_SENT";
    BroadcastReceiver br;
    FbProfile profile;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = true;
        game = new MainGame(this);
        profile = new FbProfile(this);
       // SMSReceive.context = this;
//        setBroadcastReceiver();
        initialize(game, cfg);
    }
    
    public void setBroadcastReceiver(){
    	br = new BroadcastReceiver(){
            @Override
            public void onReceive(Context ctx, Intent intent) {
                switch (getResultCode())
                {
                     case Activity.RESULT_OK:
                         Toast.makeText(getBaseContext(), "SMS sent",
                                 Toast.LENGTH_SHORT).show();
                         break;
                     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                         Toast.makeText(getBaseContext(), "SMS: Generic failure",
                                 Toast.LENGTH_SHORT).show();
                         break;
                     case SmsManager.RESULT_ERROR_NO_SERVICE:
                         Toast.makeText(getBaseContext(), "SMS: No service",
                                 Toast.LENGTH_SHORT).show();
                         break;
                     case SmsManager.RESULT_ERROR_NULL_PDU:
                         Toast.makeText(getBaseContext(), "SMS: Null PDU",
                                 Toast.LENGTH_SHORT).show();
                         break;
                     case SmsManager.RESULT_ERROR_RADIO_OFF:
                         Toast.makeText(getBaseContext(), "SMS: Radio off",
                                 Toast.LENGTH_SHORT).show();
                         break;
                }
                unregisterReceiver(br);
            }
        };
    }
    
    public void onAction(int event) {
    	switch(event){
	    	case Settings.EXIT: 	showExitDialog();	break;
	    	case Settings.SMS: 		showSMSDialog();	break;
	    	case Settings.SHARE_FB: showShareDialog();	break;
	    	case Settings.SHOW_MSG: showMessageDialog();	break;
    	}
    }
    
    public void sendMessage(){
       	//registerReceiver(br, new IntentFilter(SENT));
    }
    
    private void showMessageDialog() {
    	runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder alertboxShare = new AlertDialog.Builder(MainActivity.this);
				alertboxShare.setTitle(Settings.msg1);
				alertboxShare.setMessage(Settings.msg1);
				alertboxShare.setCancelable(false);
				alertboxShare.setNegativeButton("Không",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Assets.playSound(Assets.sound_button);
								if(Settings.delmsg){
									SMSReceive.deleteMessage();
									Settings.delmsg = false;
								}
								dialog.cancel();
							}
						});
				alertboxShare.show();
			}
		});
	}
    
    private void showSMSDialog() {
    	runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder alertboxShare = new AlertDialog.Builder(MainActivity.this);
				alertboxShare.setTitle("Thông báo");
				alertboxShare.setMessage("Bạn có muốn mua Sò bằng tin nhắn?");
				alertboxShare.setCancelable(false);
				alertboxShare.setPositiveButton("Có",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Assets.playSound(Assets.sound_button);
								sendMessage();
							}
						}).setNegativeButton("Không",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Assets.playSound(Assets.sound_button);
								dialog.cancel();
							}
						});
				alertboxShare.show();
			}
		});
	}
    
    private void showShareDialog() {
    	runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder alertboxShare = new AlertDialog.Builder(MainActivity.this);
				alertboxShare.setTitle("Thông báo");
				alertboxShare.setMessage("Bạn có muốn chia sẻ với bạn bè trên Facebook?");
				alertboxShare.setCancelable(false);
				alertboxShare.setPositiveButton("Có",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Assets.playSound(Assets.sound_button);
								profile.postLinkToWall();
							}
						}).setNegativeButton("Không",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Assets.playSound(Assets.sound_button);
								dialog.cancel();
							}
						});
				alertboxShare.show();
			}
		});
	}
    
    private void showExitDialog() {
    	runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setMessage("Bạn muốn thoát khỏi chương trình?")
						.setCancelable(false)
						.setPositiveButton("Có",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										Assets.playSound(Assets.sound_button);
										Settings.save();
										System.exit(0);
									}
								})
						.setNegativeButton("Không",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int id) {
										Assets.playSound(Assets.sound_button);
										dialog.cancel();
									}
								});
				AlertDialog alert = builder.create();
				alert.show();
			}
		});
	}
    
    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		profile.callBack(requestCode, resultCode, data);
	}
	
    @Override
    public void onBackPressed() {
    	game.onBackPressed();
    }

	@Override
	public FileHandleResolver getFileHandleResolver() {
		// TODO Auto-generated method stub
		return null;
	}
}