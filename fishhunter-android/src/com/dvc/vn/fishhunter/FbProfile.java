package com.dvc.vn.fishhunter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class FbProfile {

	// Your Facebook APP ID
	private Facebook facebook;
	private AsyncFacebookRunner mAsyncRunner;
	String FILENAME = "AndroidSSO_data";
	private SharedPreferences mPrefs;
	private JSONObject user;
	private Activity context;

	public FbProfile(Activity context) {
		this.context = context;
		facebook = new Facebook(context.getString(R.string.app_id));
		mAsyncRunner = new AsyncFacebookRunner(facebook);
		mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		// getProfileInformation();
	}

	/**
	 * Function to login into facebook
	 * */
	public void loginToFacebook() {
		String access_token = mPrefs.getString("access_token", null);
		System.out.println("Access_token: " + access_token);
		long expires = mPrefs.getLong("access_expires", 0);
		if (access_token != null) {
			facebook.setAccessToken(access_token);
			Log.d("FB Sessions", "" + facebook.isSessionValid());
		}

		if (expires != 0) {
			facebook.setAccessExpires(expires);
		}

		if (!facebook.isSessionValid()) {
			facebook.authorize(context, new String[] { "publish_stream", },
					new DialogListener() {

						@Override
						public void onCancel() {
//							showToast("cancel");
						}

						@Override
						public void onComplete(Bundle values) {
							showToast("Đăng nhập thành công");
							// Function to handle complete event
							// Edit Preferences and update facebook acess_token
							saveAccessToken();
							System.out.println("AA" + facebook.getAccessToken());
						}

						@Override
						public void onError(DialogError error) {
							showToast("Có lỗi khi đăng nhập");
						}

						@Override
						public void onFacebookError(FacebookError fberror) {
						}

					});
		}
		saveAccessToken();
	}

	public void saveAccessToken() {
		SharedPreferences.Editor editor = mPrefs.edit();
		editor.putString("access_token", facebook.getAccessToken());
		editor.putLong("access_expires", facebook.getAccessExpires());
		editor.commit();
	}

	public void showToast(final String msg) {
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(context, msg, 1).show();

			}
		});
	}

	public void callBack(int requestCode, int resultCode, Intent data) {
		facebook.authorizeCallback(requestCode, resultCode, data);
	}

	/**
	 * Get Profile information by making request to Facebook Graph API
	 * */
	public void getProfileInformation() {
		loginToFacebook();
		mAsyncRunner.request("me", new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
				Log.d("Profile", response);
				String json = response;
				try {
					user = new JSONObject(json);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}

	public void postScoreToWall(int score) {
		loginToFacebook();
		try {
			ApplicationInfo ai;
			ai = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA);

			final Bundle bundle1 = ai.metaData;
			final Bundle params = new Bundle();
			String name = "Mình";
			if (user != null)
				name = user.getString("name");
			params.putString("name", context.getString(R.string.app_name));
			params.putString(
					"description",
					name + " chơi " + context.getString(R.string.app_name)
							+ " đạt được " + score
							+ " điểm. Các bạn hãy vượt qua điểm số của "
							+ name.toLowerCase() + " đi" + ".\n"
							+ context.getString(R.string.app_name) + "  "
							+ bundle1.getString("description"));
			params.putString("picture", bundle1.getString("icon_link"));
			params.putString("link", bundle1.getString("url_click"));
			facebook.dialog(context, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError e) {
				}

				@Override
				public void onError(DialogError e) {
				}

				@Override
				public void onComplete(Bundle values) {
					saveAccessToken();
				}

				@Override
				public void onCancel() {
				}
			});
		} catch (final NameNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Function to post to facebook wall
	 * */
	public void postLinkToWall() {
		// post on user's wall.
		loginToFacebook();
		try {
			ApplicationInfo ai;
			ai = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA);

			final Bundle bundle1 = ai.metaData;
			final Bundle params = new Bundle();
			String name = "Mình";
			if (user != null)
				name = user.getString("name");
			params.putString("name", context.getString(R.string.app_name));
			params.putString("description",
					name + " đang chơi " + context.getString(R.string.app_name)
							+ ".\n" + context.getString(R.string.app_name)
							+ "  " + bundle1.getString("description"));
			params.putString("picture", bundle1.getString("icon_link"));
			params.putString("link", bundle1.getString("url_click"));

			facebook.dialog(context, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError e) {
				}

				@Override
				public void onError(DialogError e) {
				}

				@Override
				public void onComplete(Bundle values) {
					saveAccessToken();
				}

				@Override
				public void onCancel() {
				}
			});
		} catch (final NameNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void postLinkToWallAndGetGift(final int giftValue) {
		// post on user's wall.
		loginToFacebook();
		try {
			ApplicationInfo ai;
			ai = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), PackageManager.GET_META_DATA);

			final Bundle bundle1 = ai.metaData;
			final Bundle params = new Bundle();
			String name = "Mình";
			if (user != null)
				name = user.getString("name");
			params.putString("name", context.getString(R.string.app_name));
			params.putString("description", name + " vừa nhận " + giftValue
					+ " xu thưởng từ " + context.getString(R.string.app_name)
					+ ".\n" + context.getString(R.string.app_name) + "  "
					+ bundle1.getString("description"));
			params.putString("picture", bundle1.getString("icon_link"));
			params.putString("link", bundle1.getString("url_click"));
			facebook.dialog(context, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError e) {
					showToast("Có lỗi xảy ra. Hãy chia sẻ lại để nhận xu thưởng");
				}

				@Override
				public void onError(DialogError e) {
					showToast("Có lỗi xảy ra. Hãy chia sẻ lại để nhận xu thưởng");
				}

				@Override
				public void onComplete(Bundle values) {
					saveAccessToken();
//					Model.getInstance().setCoin(
//							Model.getInstance().getCoin() + giftValue);
//					showToast("Bạn đã nhận được " + giftValue + " xu!");
				}

				@Override
				public void onCancel() {
				}
			});
		} catch (final NameNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Function to show Access Tokens
	 * */
	public void showAccessTokens() {
		String access_token = facebook.getAccessToken();
	}

	/**
	 * Function to Logout user from Facebook
	 * */
	public void logoutFromFacebook() {
		mAsyncRunner.logout(context, new RequestListener() {
			@Override
			public void onComplete(String response, Object state) {
			}

			@Override
			public void onIOException(IOException e, Object state) {
			}

			@Override
			public void onFileNotFoundException(FileNotFoundException e,
					Object state) {
			}

			@Override
			public void onMalformedURLException(MalformedURLException e,
					Object state) {
			}

			@Override
			public void onFacebookError(FacebookError e, Object state) {
			}
		});
	}

}