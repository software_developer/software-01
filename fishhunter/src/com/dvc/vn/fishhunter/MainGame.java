package com.dvc.vn.fishhunter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;

public class MainGame extends Game {
	
	LoadScreen loadscreen;
	GameScreen gamescreen;
	TestScreen testscreen;
	ShopScreen shopscreen;
	MenuScreen menuscreen;
	DiaryScreen diaryscreen;
	HelpScreen helpscreen;
	public static IContact icontact;
	
	public MainGame(IContact icontact){
		this.icontact = icontact;
	}
	
	public void createScreen(){
		if(gamescreen==null){
			gamescreen = new GameScreen(this);
		}
		if(menuscreen==null){
			menuscreen = new MenuScreen(this);
		}
		if(testscreen==null){
			testscreen = new TestScreen(this);
		}
		if(shopscreen==null){
			shopscreen = new ShopScreen(this);
		}
		if(diaryscreen==null){
			diaryscreen = new DiaryScreen(this);
		}
		if(helpscreen==null){
			helpscreen = new HelpScreen(this);
		}
	}
	
	@Override
	public void create() {
		Texture.setEnforcePotImages(false);
		if(loadscreen==null)	loadscreen = new LoadScreen(this);
		setScreen(loadscreen);
	}

	@Override
	public void dispose() {
		Settings.save();
	}

	@Override
	public void render() {		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
		if(getScreen()==gamescreen){
			Settings.ispause = true;
			Settings.save();
		}
	}

	@Override
	public void resume() {
		super.resume();
	}
	
	public void onBackPressed(){
		if(getScreen()==gamescreen)
			Settings.ispause = true;
		else if(getScreen()==helpscreen)
			setScreen(menuscreen);
		else if(getScreen()==diaryscreen)
			setScreen(menuscreen);
		else if(getScreen()==shopscreen){
			if(shopscreen.isFromGame())
				setScreen(gamescreen);
			else  setScreen(menuscreen);
		}
		else if(getScreen()==menuscreen)
			icontact.onAction(Settings.EXIT);
		
	}
}
