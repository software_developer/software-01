package com.dvc.vn.fishhunter;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;

public interface IContact {
	FileHandleResolver getFileHandleResolver();
	void onAction(int event);
}
