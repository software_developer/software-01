package com.dvc.vn.fishhunter;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.dvc.vn.fishhunter.object.AnimationObject;
import com.dvc.vn.fishhunter.object.ButtonMenuObject;
import com.dvc.vn.fishhunter.ultility.OverlapTester;
import com.dvc.vn.fishhunter.ultility.SaveGame;

public class MenuScreen implements Screen, InputProcessor{

	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	private AnimationObject ani;
	private float y_anchor;
	private float aColor;
	private float y_name;
	private boolean direction;
	private ButtonMenuObject btList[];
	private boolean start,diary,help,equip,music;
	
	public MenuScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
	}
	
	public void draw(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(1, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		batcher.draw(Assets.bgMenu, 0, 0);
		batcher.draw(Assets.anchor, 360, y_anchor);
		if(y_anchor <= 70){
			batcher.draw(ani.getFrameImage(), ani.getX(), ani.getY());
		}
		else{
			batcher.draw(Assets.dust[0], ani.getX(), ani.getY());
		}
		if(y_anchor == 70){
			batcher.setColor(1, 1, 1, aColor);
			batcher.draw(Assets.nameGame, 30, y_name);
			batcher.setColor(1, 1, 1, 1);
		}
		if(direction && y_name == 300){
			for(int i = 0; i < btList.length; i++){
				if(btList[i].isDraw())
					batcher.draw(btList[i].getImage(), btList[i].getX()-btList[i].getWidth()/2,
							btList[i].getY()-btList[i].getHeight()/2,
							 btList[i].getWidth(), btList[i].getHeight());
			}
		}
		
		batcher.end();
	}
	
	public void update(float delta){
		if(y_anchor > 70) y_anchor -= delta*250;
		else y_anchor = 70;
		if(y_anchor <= 70)	ani.update(delta);
		if(y_anchor == 70){
			if(aColor==.1f)	Assets.playSound(Assets.sound_anchorfall);
			if(aColor < 0.9) aColor += delta;
			else aColor = 1;
			if(aColor == 1){
				if(!direction){
					if(y_name > 285) y_name -= 60*delta;
					else {
						y_name = 285;
						direction = true;
					}
				} else{
					if(y_name < 300) y_name += 60*delta;
					else {
						y_name = 300;
						for(int i = 0; i < btList.length; i++){
							btList[i].update(delta);
						}
					}
				}
			}
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos = new Vector3();
		guiCam.unproject(touchpos.set(Gdx.input.getX(),Gdx.input.getY(), 0));
		for(int i = 0; i < btList.length; i++){
			if (OverlapTester.pointInRectangle(btList[i].getRec(), touchpos.x ,touchpos.y)) {
				Assets.playSound(Assets.sound_button);
				switch (i) {
				case 0:
					start = true;
					break;
				case 1:
					diary = true;
					break;
				case 2:
					equip = true;
					break;
				case 3:
					music = true;
					break;
				case 4:
					help = true;
					break;

				default:
					break;
				}
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(start){
			start = false;
			game.setScreen(game.gamescreen);
		}
		if(diary){
			diary = false;
			game.setScreen(game.diaryscreen);
		}
		if(equip){
			equip = false;
			game.shopscreen.notfromGame();
			game.setScreen(game.shopscreen);
		}
		if(music){
			music = false;
			if(Settings.soundEnabled) {
				Settings.soundEnabled = false;
				btList[3].setImage(Assets.btMusicOff);
				Assets.music_backmusic.pause();
			}
			else {
				Settings.soundEnabled = true;
				btList[3].setImage(Assets.btMusicOn);
				Assets.music_backmusic.play();
			}
		}
		if(help){
			help = false;
			game.setScreen(game.helpscreen);
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(game.menuscreen);
		for(int i=0;i<9;i++){
			if(i<7)	GunManager.gun[i].setState(SaveGame.getInt("state"+i, 1));
			GunManager.gun[i].setDameUps(SaveGame.getInt("dameups"+i, 0));
			GunManager.gun[i].setSpeedUps(SaveGame.getInt("speedups"+i, 0));
		}
		GunManager.gun[7].setState(SaveGame.getInt("state7", 2));
		GunManager.gun[8].setState(SaveGame.getInt("state8", 3));
		ani = new AnimationObject(Assets.dust, 276, 55, .7f, 0, 0, true);
		ani.setStartFrame(1);
		y_anchor = 480;
		y_name = 300;
		direction = false;
		aColor = 0.1f;
		btList = new ButtonMenuObject[5];
		btList[0] = new ButtonMenuObject(Assets.btStart, 160, 160, 0.25f);
		btList[1] = new ButtonMenuObject(Assets.btDiary, 460, 100, 0.5f);
		btList[2] = new ButtonMenuObject(Assets.btEquip, 610, 140, 0.75f);
		btList[3] = new ButtonMenuObject(Assets.btMusicOn, 690, 270, 1);
		btList[4] = new ButtonMenuObject(Assets.btHelp, 600, 370, 1.25f);
		start = false;
		diary = false;
		equip = false;
		music = false;
		help = false;
		if(Settings.soundEnabled) {
			btList[3].setImage(Assets.btMusicOn);
		}
		else {
			btList[3].setImage(Assets.btMusicOff);
		}
	}
	
	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
