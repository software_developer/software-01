package com.dvc.vn.fishhunter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Assets {
	public static AssetManager manager;
	//GameScreen
	public static Texture[] scene;
	public static TextureRegion lasergun;
	public static TextureRegion[] gun;
	public static TextureRegion[] gunhead;
	public static TextureRegion[] bullet;
	public static TextureRegion[] net;
	public static TextureRegion content;
	public static TextureRegion[][] smallFish;
	public static TextureRegion[][] bigFish;
	public static TextureRegion[] shark;
	public static TextureRegion[] fairy;
	public static TextureRegion[] coinsbonnus;
	public static TextureRegion contentCoin;
	public static TextureRegion contentShell;
	public static TextureRegion contentPower;
	public static TextureRegion contentLevel;
	public static TextureRegion circleLevel;
	public static TextureRegion expball;
	public static TextureRegion[] lasershot;
	public static TextureRegion[] laserlight;
	public static TextureRegion[] lasereff1;
	public static TextureRegion[] lasereff2;
	public static TextureRegion[] lasereff3;
	public static TextureRegion[] wave;
	public static TextureRegion levelUp_congrate;
	public static TextureRegion star;
	public static TextureRegion table;
	public static TextureRegion bigShell;
	public static TextureRegion pause_bg;
	public static TextureRegion pause_sound_on;
	public static TextureRegion pause_sound_off;
	public static TextureRegion pause_continue_normal;
	public static TextureRegion pause_continue_push;
	public static TextureRegion pause_menu_normal;
	public static TextureRegion pause_menu_push;
	public static Texture power;
	public static TextureRegion help;
	
	
	//Items
	public static TextureRegion effectCircle;
	public static TextureRegion itemBomb;
	public static TextureRegion itemBread;
	public static TextureRegion itemShock;
	public static TextureRegion bubble;
	public static TextureRegion[] bombeffect;
	public static TextureRegion[] shockeff1;
	public static TextureRegion[] shockeff2;
	public static TextureRegion[] shockfish;
	public static TextureRegion[] breadeff;
	
	//ShopScreen
	public static TextureRegion[] itemGun;
	public static TextureRegion[] itemMoney;
	public static TextureRegion[] item;
	public static TextureRegion iconCoin;
	public static TextureRegion iconCoinAdd;
	public static TextureRegion iconShell;
	public static TextureRegion iconShellAdd;
	public static TextureRegion lock;
	public static TextureRegion shopChoice1;
	public static TextureRegion shopChoice2;
	public static TextureRegion contentShopInfo;
	public static TextureRegion contentShop1;
	public static TextureRegion contentShop1Select;
	public static TextureRegion contentShop1Disable;
	public static TextureRegion contentShop1Choice;
	public static TextureRegion contentShop2;
	public static TextureRegion contentShop2Select;
	public static TextureRegion contentShop2Disable;
	public static TextureRegion contentShop2Choice;
	public static TextureRegion btExit;
	public static TextureRegion btExitPress;
	
	//Gift Screen
	public static TextureRegion contentShop;
	public static TextureRegion fishCord;
	public static TextureRegion prizes;
	public static TextureRegion listbonus;
	public static TextureRegion buttonSpin;
	public static TextureRegion buttonSpinPrs;
	public static TextureRegion buttonStop;
	public static TextureRegion buttonStopPrs;
	public static TextureRegion buttonStopDisable;
	public static TextureRegion circle;
	public static TextureRegion round;
	public static TextureRegion next;
	public static TextureRegion nextPrs;
	public static TextureRegion prev;
	public static TextureRegion prevPrs;
	
	public static TextureRegion treasure;
	public static TextureRegion nami;
	public static TextureRegion eyeNami;
	public static TextureRegion trunk;
	public static TextureRegion trunkGold;
	public static TextureRegion trunkBigGold;
	public static TextureRegion decoration;
	public static TextureRegion effectTrunk;
	public static TextureRegion btnEquip;
	public static TextureRegion[] coinAni;
	
	//Menu Screen
	public static TextureRegion bgMenu;
	public static TextureRegion anchor;
	public static TextureRegion nameGame;
	public static TextureRegion btStart;
	public static TextureRegion btDiary;
	public static TextureRegion btEquip;
	public static TextureRegion btHelp;
	public static TextureRegion btMusicOn;
	public static TextureRegion btMusicOff;
	public static TextureRegion[] dust;
	
	//Diary Screen
	public static TextureRegion diarybg;
	public static TextureRegion diary_btnShare;
	public static TextureRegion diary_btnSharePressed;
	public static TextureRegion diary_nami;
	public static TextureRegion[] diary_fish;
	
	//font
	public static BitmapFont fontSimple;
	public static BitmapFont fontBigMoney;
	public static BitmapFont fontSmallMoney;
	public static BitmapFont fontCountTime;
	public static BitmapFont fontLevel;
	public static BitmapFont fontItem;
	public static BitmapFont fontShell;
	public static BitmapFont fontdiarybig;
	public static BitmapFont fontdiarysmall;
	
	public static Music music_backmusic;
	public static Sound sound_anchorfall;
	public static Sound sound_bubble;
	public static Sound sound_button;
	public static Sound sound_catchNamy;
	public static Sound sound_changeGun;
	public static Sound sound_coins;
	public static Sound sound_laserPrompt;
	public static Sound sound_lasershot;
	public static Sound sound_netActive;
	public static Sound sound_shot;
	public static Sound sound_tip;
	public static Sound sound_Bomb;
	public static Sound sound_Bread;
	public static Sound sound_Shock;
	public static Sound sound_treasureGain;
	public static Sound sound_wave1;
	public static Sound sound_wave2;
	
	public static void playSound (Sound sound) {
		if (sound!=null && Settings.soundEnabled) sound.play(1);
	}
	
	public static void loadSound(){
		music_backmusic = Gdx.audio.newMusic(Gdx.files.internal("sound/backmusic.ogg"));
		music_backmusic.setLooping(true);
		sound_anchorfall = Gdx.audio.newSound(Gdx.files.internal("sound/anchor_fall.ogg"));
		sound_bubble = Gdx.audio.newSound(Gdx.files.internal("sound/bubble.ogg"));
		sound_button = Gdx.audio.newSound(Gdx.files.internal("sound/button.ogg"));
		sound_catchNamy = Gdx.audio.newSound(Gdx.files.internal("sound/catch_seamaiden.ogg"));
		sound_changeGun = Gdx.audio.newSound(Gdx.files.internal("sound/change_gun.ogg"));
		sound_coins = Gdx.audio.newSound(Gdx.files.internal("sound/coins.wav"));
		sound_laserPrompt = Gdx.audio.newSound(Gdx.files.internal("sound/laser_prompt.ogg"));
		sound_lasershot = Gdx.audio.newSound(Gdx.files.internal("sound/laser_shoot.ogg"));
		sound_netActive = Gdx.audio.newSound(Gdx.files.internal("sound/net_active.ogg"));
		sound_shot = Gdx.audio.newSound(Gdx.files.internal("sound/shot.wav"));
		sound_tip = Gdx.audio.newSound(Gdx.files.internal("sound/tip.ogg"));
		sound_Bomb = Gdx.audio.newSound(Gdx.files.internal("sound/tools_bomb.ogg"));
		sound_Bread = Gdx.audio.newSound(Gdx.files.internal("sound/tools_bread.ogg"));
		sound_Shock = Gdx.audio.newSound(Gdx.files.internal("sound/tools_shork.mp3"));
		sound_treasureGain = Gdx.audio.newSound(Gdx.files.internal("sound/treasure_gains.ogg"));
		sound_wave1 = Gdx.audio.newSound(Gdx.files.internal("sound/wave_1.ogg"));
		sound_wave2 = Gdx.audio.newSound(Gdx.files.internal("sound/wave_2.ogg"));
	}
	
	public static void load(){
		
	}
	
	public static void loadGame(){
		
		if (manager.update()) {
			TextureAtlas diaryAtlas = manager.get("Image/diary/diary.pack", TextureAtlas.class);
			TextureAtlas helpAtlas = manager.get("Image/help/help.pack", TextureAtlas.class);
			TextureAtlas menuAtlas = manager.get("Image/menu/menu.pack", TextureAtlas.class);
			TextureAtlas gameAtlas = manager.get("Image/game/game.pack", TextureAtlas.class);
			TextureAtlas bonusAtlas = manager.get("Image/game/bonus/bonus.pack", TextureAtlas.class);
			TextureAtlas fishAtlas = manager.get("Image/game/fish/fish.pack", TextureAtlas.class);
			TextureAtlas gunAtlas = manager.get("Image/game/gun/gun.pack", TextureAtlas.class);
			TextureAtlas itemAtlas = manager.get("Image/game/items/item.pack", TextureAtlas.class);
			TextureAtlas shopAtlas = manager.get("Image/game/shop/shop.pack", TextureAtlas.class);
			
			scene = new Texture[4];
			for(int i=0;i<4;i++){
				scene[i] = manager.get("Image/scene/scene"+(i+1)+".jpg",Texture.class);
			}
			
			coinsbonnus = new TextureRegion[8];
			for(int i=0;i<8;i++){
				coinsbonnus[i] = bonusAtlas.findRegion("coin-bonus-"+(i+1));
			}
			
			smallFish = new TextureRegion[5][18];
			for(int k=0;k<5;k++)
				for(int i=0;i<18;i++){
					smallFish[k][i] = fishAtlas.findRegion("smallfish"+(k+1)+"-"+(i+1));
				}
			bigFish = new TextureRegion[6][18];
			for(int k=0;k<6;k++)
				for(int i=0;i<18;i++){
					bigFish[k][i] = fishAtlas.findRegion("bigfish"+(k+1)+"-"+(i+1));
				}
			shark = new TextureRegion[18];
			for(int i=0;i<18;i++){
				shark[i] = fishAtlas.findRegion("shark-"+(i+1));
			}
			fairy = new TextureRegion[21];
			for(int i=0;i<21;i++){
				fairy[i] = fishAtlas.findRegion("fairy-"+(i+1));
			}
			lasergun = gunAtlas.findRegion("gun");
			gun = new TextureRegion[9];
			for(int i=0;i<9;i++){
				gun[i] = gunAtlas.findRegion("gun-"+(i+1));
			}
			
			effectCircle = itemAtlas.findRegion("circle");
			itemBomb = itemAtlas.findRegion("item-bomb");
			itemBread = itemAtlas.findRegion("item-bread");
			itemShock = itemAtlas.findRegion("item-shock");
			bubble = itemAtlas.findRegion("bubble");
			bombeffect = new TextureRegion[5];
			for(int i=0;i<5;i++){
				bombeffect[i] = itemAtlas.findRegion("bombeffect-"+(i+1));
			}
			shockeff1 = new TextureRegion[2];
			for(int i=0;i<2;i++){
				shockeff1[i] = itemAtlas.findRegion("shockeff1-"+(i+1));
			}
			shockeff2 = new TextureRegion[2];
			for(int i=0;i<2;i++){
				shockeff2[i] = itemAtlas.findRegion("shockeff2-"+(i+1));
			}
			shockfish = new TextureRegion[3];
			for(int i=0;i<3;i++){
				shockfish[i] = itemAtlas.findRegion("effect-sock-"+(i+1));
			}
			breadeff = new TextureRegion[5];
			for(int i=0;i<5;i++){
				breadeff[i] = itemAtlas.findRegion("breadeff-"+(i+1));
			}
			
			itemGun = new TextureRegion[10];
			for(int i=0;i<10;i++){
				itemGun[i] = shopAtlas.findRegion("item-gun-"+(i+1));
			}
			
			itemMoney = new TextureRegion[4];
			for(int i=0;i<4;i++){
				itemMoney[i] = shopAtlas.findRegion("item-money-"+(i+1));
			}
			
			item = new TextureRegion[4];
			for(int i=0;i<4;i++){
				item[i] = shopAtlas.findRegion("item-"+(i+1));
			}
			
			gunhead = new TextureRegion[9];
			for(int i=0;i<9;i++){
				gunhead[i] = gunAtlas.findRegion("gunhead-"+(i+1));
			}
			
			bullet = new TextureRegion[3];
			for(int i=0;i<3;i++){
				bullet[i] = gunAtlas.findRegion("bullet-"+(i+1));
			}
			
			net = new TextureRegion[3];
			for(int i=0;i<3;i++){
				net[i] = gunAtlas.findRegion("net-"+(i+1));
			}
			coinAni = new TextureRegion[4];
			for(int i=0;i<4;i++){
				coinAni[i] = gameAtlas.findRegion("coin-ani-"+(i+1));
			}
			
			dust = new TextureRegion[5];
			for(int i=0;i<5;i++){
				dust[i] = menuAtlas.findRegion("dust-"+(i+1));
			}
			lasershot = new TextureRegion[2];
			for(int i=0;i<2;i++){
				lasershot[i] = gunAtlas.findRegion("lasershot1-"+(i+1));
			}
			laserlight = new TextureRegion[2];
			for(int i=0;i<2;i++){
				laserlight[i] = gunAtlas.findRegion("laser-light-"+(i+1));
			}
			lasereff1 = new TextureRegion[13];
			for(int i=0;i<13;i++){
				lasereff1[i] = itemAtlas.findRegion("lasereff1-"+(i+1));
			}
			lasereff2 = new TextureRegion[7];
			for(int i=0;i<7;i++){
				lasereff2[i] = itemAtlas.findRegion("lasereff2-"+(i+1));
			}
			lasereff3 = new TextureRegion[14];
			for(int i=0;i<14;i++){
				lasereff3[i] = itemAtlas.findRegion("lasereff4-"+(i+1));
			}
			wave = new TextureRegion[6];
			for(int i=0;i<6;i++){
				wave[i] = gameAtlas.findRegion("wave-"+(i+1));
			}
			
			help = helpAtlas.findRegion("help");
			contentLevel = gameAtlas.findRegion("content-level");
			circleLevel = gameAtlas.findRegion("circle-level");
			contentPower = gameAtlas.findRegion("content-power");
			expball = gameAtlas.findRegion("exp-ball");
			power = new Texture(Gdx.files.internal("Image/game/power.png"));
			star = gameAtlas.findRegion("star");
			levelUp_congrate = gameAtlas.findRegion("levelUp-congrate");
			table = gameAtlas.findRegion("table");
			bigShell = gameAtlas.findRegion("bigShell");
			pause_bg = gameAtlas.findRegion("pause-bg");
			pause_sound_on = gameAtlas.findRegion("pause-sound-normal");
			pause_sound_off = gameAtlas.findRegion("pause-sound-push");
			pause_continue_normal = gameAtlas.findRegion("pause-continue-normal");
			pause_continue_push = gameAtlas.findRegion("pause-continue-push");
			pause_menu_normal = gameAtlas.findRegion("pause-menu-normal");
			pause_menu_push = gameAtlas.findRegion("pause-menu-push");
			contentCoin = gameAtlas.findRegion("content-coin");
			contentShell = gameAtlas.findRegion("content-shell");
			content = gameAtlas.findRegion("content");
			prev = gunAtlas.findRegion("next");
			prevPrs = gunAtlas.findRegion("next-prs");
			next = new TextureRegion(prev);
			nextPrs = new TextureRegion(prevPrs);
			next.flip(true, false);
			nextPrs.flip(true, false);
			
			contentShop = bonusAtlas.findRegion("content-shop");
			fishCord = bonusAtlas.findRegion("fish-cord");
			prizes = bonusAtlas.findRegion("prizes");
			listbonus = bonusAtlas.findRegion("list-bonus");
			buttonSpin = bonusAtlas.findRegion("button-spin");
			buttonSpinPrs = bonusAtlas.findRegion("button-spin-prs");
			buttonStop = bonusAtlas.findRegion("button-stop");
			buttonStopPrs = bonusAtlas.findRegion("button-stop-prs");
			buttonStopDisable = bonusAtlas.findRegion("button-stop-disable");
			circle = bonusAtlas.findRegion("circle");
			round = bonusAtlas.findRegion("round");
			
			nami = bonusAtlas.findRegion("nami");
			decoration = bonusAtlas.findRegion("decoration");
			eyeNami = bonusAtlas.findRegion("eyes-nami");
			trunk = bonusAtlas.findRegion("trunk");
			trunkGold = bonusAtlas.findRegion("trunk-gold");
			trunkBigGold = bonusAtlas.findRegion("trunk-biggold");
			treasure = bonusAtlas.findRegion("treasure");
			effectTrunk = bonusAtlas.findRegion("effect");
			btnEquip = gameAtlas.findRegion("bt-item");
		
			lock = shopAtlas.findRegion("item-lock");
			iconCoin = shopAtlas.findRegion("coin");
			iconCoinAdd = shopAtlas.findRegion("coin-add");
			iconShell = shopAtlas.findRegion("clamshell");
			iconShellAdd = shopAtlas.findRegion("clamshell-add");
			contentShopInfo = shopAtlas.findRegion("content-info");
			contentShop1 = shopAtlas.findRegion("content-1");
			contentShop1Select = shopAtlas.findRegion("content-1-select");
			contentShop1Disable = shopAtlas.findRegion("content-1-disable");
			contentShop1Choice = shopAtlas.findRegion("content-1-choice");
			contentShop2 = shopAtlas.findRegion("content-2");
			contentShop2Select = shopAtlas.findRegion("content-2-select");
			contentShop2Disable = shopAtlas.findRegion("content-2-disable");
			contentShop2Choice = shopAtlas.findRegion("content-2-choice");
			shopChoice1 = shopAtlas.findRegion("item-choice-1");
			shopChoice2 = shopAtlas.findRegion("item-choice-2");
			btExit = shopAtlas.findRegion("button-exit");
			btExitPress = shopAtlas.findRegion("button-exit-press");
			
			bgMenu = menuAtlas.findRegion("bg");
			anchor = menuAtlas.findRegion("anchor");
			nameGame = menuAtlas.findRegion("name-game");
			btStart = menuAtlas.findRegion("bt-start");
			btDiary = menuAtlas.findRegion("bt-diary");
			btEquip = menuAtlas.findRegion("bt-equip");
			btHelp = menuAtlas.findRegion("bt-help");
			btMusicOn = menuAtlas.findRegion("bt-music-on");
			btMusicOff = menuAtlas.findRegion("bt-music-off");
			
			diarybg = diaryAtlas.findRegion("diarybg");
			diary_btnShare = diaryAtlas.findRegion("button-share");
			diary_btnSharePressed = diaryAtlas.findRegion("button-share-press");
			diary_nami = diaryAtlas.findRegion("nami");
			diary_fish = new TextureRegion[13];
			for(int i=0;i<13;i++){
				diary_fish[i] = diaryAtlas.findRegion("fish-"+(i+1)+"");
			}
			
			fontSimple = new BitmapFont(Gdx.files.internal("font/font-simple.fnt"), Gdx.files.internal("font/font-simple.png"), false);
			fontBigMoney = new BitmapFont(Gdx.files.internal("font/font-big-money.fnt"), Gdx.files.internal("font/font-big-money.png"), false);
			fontSmallMoney = new BitmapFont(Gdx.files.internal("font/font-small-money.fnt"), Gdx.files.internal("font/font-small-money.png"), false);
			fontCountTime = new BitmapFont(Gdx.files.internal("font/font-count-time.fnt"), Gdx.files.internal("font/font-count-time.png"), false);
			fontLevel = new BitmapFont(Gdx.files.internal("font/font-level.fnt"), Gdx.files.internal("font/font-level.png"), false);
			fontItem = new BitmapFont(Gdx.files.internal("font/font-item.fnt"), Gdx.files.internal("font/font-item.png"), false);
			fontShell = new BitmapFont(Gdx.files.internal("font/font-shell.fnt"), Gdx.files.internal("font/font-shell.png"), false);
			fontdiarybig = new BitmapFont(Gdx.files.internal("font/font-diary-big.fnt"), Gdx.files.internal("font/font-diary-big.png"), false);
			fontdiarysmall = new BitmapFont(Gdx.files.internal("font/font-diary-small.fnt"), Gdx.files.internal("font/font-diary-small.png"), false);
			LoadScreen.count = 4;
		}
	}
}
