package com.dvc.vn.fishhunter;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.dvc.vn.fishhunter.ultility.OverlapTester;

public class DiaryScreen implements Screen, InputProcessor{
	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	
	Rectangle btn_exit, btn_share;
	TextureRegion btnExit, btnShare;
	
	public DiaryScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
		touchpos = new Vector3();
		
		btn_exit = new Rectangle(760, 440, 35, 35);
		btn_share = new Rectangle(320, 20, 163, 47);
		btnExit = Assets.btExit;
		btnShare = Assets.diary_btnShare;
	}
	
	public void update(float delta){
		
	}
	
	public void draw(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		
		batcher.draw(Assets.diarybg, 0, 0);
		batcher.draw(Assets.diary_nami, 640, 50);
		batcher.draw(btnExit, btn_exit.x, btn_exit.y);
		batcher.draw(btnShare, btn_share.x, btn_share.y);
		
		for(int i=0;i<13;i++){
			drawFish(i, 120 + (i/5)*220, 350-(i%5)*60);
		}
		
		batcher.end();
	}
	
	public void drawFish(int i, int x, int y){
		batcher.draw(Assets.diary_fish[i], x-Assets.diary_fish[i].getRegionWidth(), y-Assets.diary_fish[i].getRegionHeight()/2);
		if(i==12)	Assets.fontdiarybig.draw(batcher, " x "+Settings.fishs[i], x, y+45);
		else	Assets.fontdiarysmall.draw(batcher, " x "+Settings.fishs[i], x, y+20);
		
	}

	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		guiCam.unproject(touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		x = (int)touchpos.x;
		y = (int)touchpos.y;
		if(OverlapTester.pointInRectangle(btn_exit, x, y)){
			Assets.playSound(Assets.sound_button);
			btnExit = Assets.btExitPress;
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_share, x, y)){
			Assets.playSound(Assets.sound_button);
			btnShare = Assets.diary_btnSharePressed;
			return false;
		}
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		guiCam.unproject(touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		x = (int)touchpos.x;
		y = (int)touchpos.y;
		
		if(OverlapTester.pointInRectangle(btn_exit, x, y)){
			btnExit = Assets.btExit;
			game.setScreen(game.menuscreen);
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_share, x, y)){
			btnShare = Assets.diary_btnShare;
			if(game.icontact!=null)		game.icontact.onAction(Settings.SHARE_FB);
			return false;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(game.diaryscreen);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
	
}
