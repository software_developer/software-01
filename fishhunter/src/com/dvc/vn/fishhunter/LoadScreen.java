package com.dvc.vn.fishhunter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;

public class LoadScreen implements Screen{

	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	Texture load0;
	Texture load1;
	public static int count;
	
	public LoadScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
		touchpos = new Vector3();
		count = 1;
		load();
	}
	
	public void load(){
		Assets.manager = new AssetManager();
		load0 = new Texture(Gdx.files.internal("loading/loading.png"));
		load1 = new Texture(Gdx.files.internal("loading/loading-bote.png"));
		Assets.manager.load("Image/game/power.png", Texture.class);
		Assets.manager.load("Image/scene/scene1.jpg", Texture.class);
		Assets.manager.load("Image/scene/scene2.jpg", Texture.class);
		Assets.manager.load("Image/scene/scene3.jpg", Texture.class);
		Assets.manager.load("Image/scene/scene4.jpg", Texture.class);
		Assets.manager.load("Image/diary/diary.pack", TextureAtlas.class);
		Assets.manager.load("Image/help/help.pack", TextureAtlas.class);
		Assets.manager.load("Image/menu/menu.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/game.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/bonus/bonus.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/fish/fish.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/gun/gun.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/items/item.pack", TextureAtlas.class);
		Assets.manager.load("Image/game/shop/shop.pack", TextureAtlas.class);
		Assets.loadSound();
		Settings.load();
		if(Settings.soundEnabled)	Assets.music_backmusic.play();
	}
	
	public void reset(){
		count = 0;
	}
	
	public void update(float delta){
//		count ++;
		switch(count){
			case 1: 
					Assets.loadGame();
					break;
			case 2: 
//					Assets.loadItemPandora();
					break;
			case 3: 
//					Assets.loadStageClassic();
					break;
		}
		if(count==4){
			game.createScreen();
			game.setScreen(game.menuscreen);
			
//			game.testscreen.setKindBonus(1);
//			game.setScreen(game.testscreen);
//			game.setScreen(game.shopscreen);
//			game.setScreen(game.menuscreen);
			
			//if (Settings.musicEnabled) Assets.main_music.play();
		}
	}
	
	public void draw(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(1, 1, 1, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		int q  = (int) ((210/100)*(Assets.manager.getProgress()*100));
		batcher.draw(load1, 275+q, 200);
		batcher.draw(load0, 260, 150);
		batcher.end();
	}
	
	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
	
}
