package com.dvc.vn.fishhunter;

import java.util.ArrayList;
import java.util.Random;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.dvc.vn.fishhunter.object.AnimationObject;
import com.dvc.vn.fishhunter.object.BulletObject;
import com.dvc.vn.fishhunter.object.CoinObject;
import com.dvc.vn.fishhunter.object.CoinStage;
import com.dvc.vn.fishhunter.object.FeededBread;
import com.dvc.vn.fishhunter.object.FishInfo;
import com.dvc.vn.fishhunter.object.FishObject;
import com.dvc.vn.fishhunter.object.GunObject;
import com.dvc.vn.fishhunter.object.LaserGun;
import com.dvc.vn.fishhunter.object.TwinkyStar;
import com.dvc.vn.fishhunter.ultility.OverlapTester;

public class GameScreen implements Screen, InputProcessor{

	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	Random rand;
	
	private int stateGun = 5;
	private int statenet;
	private int chosenItem;
	private int splitScene;
	private int scene;
	private float gunbonusY;
	private float waithiddenstage = .5f;
	private boolean left,right;
	private boolean gunout, gunin;
	private boolean isSplitScene;
	public float coinDraw;
	public float timeAddCoin;
	
	private GunObject gun;
	private LaserGun lasergun;
	private AnimationObject wave;
	private CoinStage levelUp_congrate;
	private Array<BulletObject> bullets;
	private ArrayList<FishObject> fishs;
	private Array<CoinObject> coins;
	private Array<AnimationObject> effs;
	private Array<AnimationObject> cireffs;
	private Array<FeededBread> breads;
	private Array<CoinStage> coinstage;
	private Array<CoinStage> shellstage;
	private Array<TwinkyStar> stars;
	private Array<Integer> hiddenStage;
	private TextureRegion item;
	private Vector3 laserline;

	private Rectangle btn_equip;
	private Rectangle leftBound, rightBound;
	private Rectangle btn_bomb, btn_bread, btn_shock;
	private Rectangle btn_sound, btn_continue, btn_menu;
	private TextureRegion  btnSound, btnContinue, btnMenu;
	
	public GameScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
		touchpos = new Vector3();
		rand = new Random();
		
		btn_equip = new Rectangle(0, 416, 68, 64);
		btn_bomb = new Rectangle(285, 410, 70, 70);
		btn_bread = new Rectangle(363, 410, 70, 70);
		btn_shock = new Rectangle(441, 410, 70, 70);
		rightBound = new Rectangle(450, -5, 54, 54);
		leftBound = new Rectangle(290, -5, 54, 54);
		
		btn_sound = new Rectangle(500, 250, 74, 70);
		btn_continue = new Rectangle(300, 220, 198, 70);
		btn_menu = new Rectangle(300, 150, 193, 63);
		btnContinue = Assets.pause_continue_normal;
		btnMenu = Assets.pause_menu_normal;
		
		bullets = new Array<BulletObject>();
		fishs = new ArrayList<FishObject>();
		coins = new Array<CoinObject>();
		effs = new Array<AnimationObject>();
		cireffs = new Array<AnimationObject>();
		breads = new Array<FeededBread>();
		coinstage = new Array<CoinStage>();
		shellstage = new Array<CoinStage>();
		stars = new Array<TwinkyStar>();
		hiddenStage = new Array<Integer>();
		timeAddCoin = 11;
		gunbonusY = 0;
	}
	
	public void updateGunFromShopping(){
		gun.updateFromShopping();
	}
	
	public void addFishs(FishInfo fishInfo, int n){
		int region = rand.nextInt(4);		//XÃ¡c Ä‘á»‹nh vÃ¹ng xh cÃ¡: trÃ¡i-pháº£i-trÃªn-dÆ°á»›i
		int x, y, r;						//tá»�a Ä‘á»™ vÃ  gÃ³c di chuyá»ƒn
		switch(region){
			case 0:	x = -50; y = rand.nextInt(480);	r = 0+rand.nextInt(60)-30; break;	//trÃ¡i;
			case 1:	x = 900; y = rand.nextInt(480);	r = 180+rand.nextInt(60)-30; break;	//pháº£i;
			case 2:	x = rand.nextInt(800); y = 0;	r = 90+rand.nextInt(60)-30; break;		//dÆ°á»›i
			default :	x = rand.nextInt(800); y = 580;	r = -90+rand.nextInt(60)-30; break;	//trÃªn
		}
		
		FishObject fish = null;
		for(int i=0;i<n;i++){
			fish = new FishObject(fishInfo, x-(i/2)*60, y-(i%2)*30, r, Settings.level*.1f);
			fishs.add(fish);
		}
	}
	
	public void createFishs(float delta){
		for(int i=0;i<FishManager.smalls.length;i++){
			if(rand.nextFloat()<FishManager.smalls[i].appear*delta/.01f){
				if(i<4)		addFishs(FishManager.smalls[i], 3 + rand.nextInt(4));
				else 		addFishs(FishManager.smalls[i], 1);
			}
		}
		for(int i=0;i<FishManager.bigs.length;i++){
			if(rand.nextFloat()<FishManager.bigs[i].appear*delta/.01f)		addFishs(FishManager.bigs[i], 1);
		}
		if(rand.nextFloat()<FishManager.shark.appear*delta/.01f)		addFishs(FishManager.shark, 1);
		if(rand.nextFloat()<FishManager.fairy.appear*delta/.01f)		addFishs(FishManager.fairy, 1);
	}
	
	public void catchFish(BulletObject b){
		for(FishObject fish: fishs){
			if(OverlapTester.pointInRectangle(b.getDamageRegion(), fish.getX(), fish.getY())){
				fish.takeDamage(b.getDamage(), true);
			}
		}
	}
	
	public void bombExplosion(int x, int y){
		Rectangle r = new Rectangle(x-150, y-150, 300, 300);
		for(FishObject fish: fishs){
			if(OverlapTester.pointInRectangle(r, fish.getX(), fish.getY())){
				fish.takeDamage(10000, false);
			}
		}
	}
	
	public void shockExplosion(int x, int y){
		Rectangle r = new Rectangle(x-150, y-150, 300, 300);
		for(FishObject fish: fishs){
			if(OverlapTester.pointInRectangle(r, fish.getX(), fish.getY())){
				fish.inShockState(5);
				effs.add(new AnimationObject(Assets.shockfish, fish.getX(), fish.getY(), .15f, 5, 0, false));
			}
		}
	}
	
	public void breadFeeding(FeededBread bread){
		for(FishObject fish: fishs){
			if(fish.getKind()<4 && OverlapTester.pointInRectangle(bread.getRectangle(), fish.getX(), fish.getY())){
				fish.setFoundBread(bread);
			}
		}
	}
	
	public void updateEffects(Array<AnimationObject> arr, float delta){
		for(AnimationObject a: arr){
			a.update(delta);
			if(a.isCompleted()){
				arr.removeValue(a, true);
			}
		}
	}
	
	public void killFish(FishObject fish){
		Settings.fishs[fish.getKind()] ++;
		if(fish.getKind()==11){
			hiddenStage.add(1);
		}
		else if(fish.getKind()==12){
			hiddenStage.add(2);
		}
		else if(fish.getKind()<11){
			Assets.playSound(Assets.sound_coins);
			coins.add(new CoinObject(Assets.coinAni, fish.getX(), fish.getY(), 0, 
					Assets.coinAni[0].getRegionWidth()/2, Assets.coinAni[0].getRegionHeight()/2, new Vector2(100, 20), .5f));
			coinstage.add(new CoinStage(fish.getX(), fish.getY(), fish.getCoins()));
			addCoin(fish.getCoins());
		}
		if(fish.isShotDead()){
			if(Settings.power<100)	Settings.power += 1;
			else{
				Settings.power = 0;
				lasergun.reset();
				gunout = true;
				Assets.playSound(Assets.sound_laserPrompt);
			}
		}
		Settings.exp ++;
		if(Settings.exp>=Settings.requiredExp){
			isSplitScene = true;
			Settings.level ++;
			Settings.requiredExp += 50;
			Settings.exp = 0;
			Settings.shell += 1;
			shellstage.add(new CoinStage(670, 100, 1));
		}
	}
	
	private void update(float delta){
		if(Settings.ispause)		return;
		if(Settings.coin < 999999999){
			timeAddCoin -= delta;
			if(timeAddCoin <= 0){
				Settings.coin++;
				timeAddCoin = 11;
				coinstage.add(new CoinStage(100, 40, 1));
			}
		}
		if(coinDraw < Settings.coin){
			coinDraw += 100*delta;
		}
		else coinDraw = Settings.coin;
		if(gunout){
			gunbonusY -= 100*delta/.1f;
			if(gunbonusY<-150){
				gunout = false;
				gunin = true;
				gun.setGunPower(GunManager.gun[stateGun]);
			}
		}
		else if(gunin){
			gunbonusY += 100*delta/.1f;
			if(gunbonusY>=0){
				gunin = false;
				gunbonusY = 0;
			}
		}
		if(isSplitScene){
			levelUp_congrate.update(delta);
			wave.update(delta);
			splitScene -= 100*delta;
			if(splitScene<0){
				isSplitScene = false;
				scene = Settings.level%2;
				levelUp_congrate.setPos(190, 200);
				levelUp_congrate.reset();
				splitScene = 800;
			}
		}
		gun.update(delta);
		lasergun.update(delta);
		createFishs(delta);
		updateEffects(effs, delta);
		updateEffects(cireffs, delta);
		for(Integer i: hiddenStage){
			waithiddenstage -= delta;
			if(waithiddenstage<=0){
				Assets.playSound(Assets.sound_catchNamy);
				waithiddenstage = .5f;
				game.testscreen.setKindBonus(i);
				game.setScreen(game.testscreen);
				hiddenStage.removeValue(i, false);
			}
		}
		for(TwinkyStar s: stars){
			s.update(delta);
			if(s.isDestroyed())		stars.removeValue(s, true);
		}
		for(BulletObject b : bullets){
			if(b.reachedTarget()){
				b.setImage(Assets.net[statenet]);
				b.explose();
				catchFish(b);
				Assets.playSound(Assets.sound_netActive);
			}
			b.update(delta);
			if(b.isDestroyed()){
				bullets.removeValue(b, true);
			}
		}
		for(CoinObject c : coins){
			if(rand.nextFloat()<.05f*delta/.01f){
				stars.add(new TwinkyStar(Assets.star, c.getX(), c.getY(), 0, 32, 32));
				stars.get(stars.size-1).randomParam(rand);
			}
			c.update(delta);
			if(c.reachedTarget()){
				coins.removeValue(c, true);
			}
		}
		for(FeededBread b: breads){
			breadFeeding(b);
			b.update(delta);
			if(b.isCompleted()){
				breads.removeValue(b, true);
			}
		}
		for(CoinStage cs: coinstage){
			cs.update(delta);
			if(cs.isTimeUp()){
				coinstage.removeValue(cs, true);
			}
		}
		for(CoinStage ss: shellstage){
			ss.update(delta);
			if(ss.isTimeUp()){
				shellstage.removeValue(ss, true);
			}
		}
		for(int i=fishs.size()-1;i>=0;i--){
			if(fishs.get(i).getX()<-350 || 
					fishs.get(i).getX()>950 || fishs.get(i).getY()<-150 || fishs.get(i).getY()>650){
				fishs.remove(i);
				continue;
			}
			float dr = rand.nextFloat();
			float sr = rand.nextFloat();
			float d = (rand.nextInt(360)-180)*(.8f-fishs.get(i).getKind()*.15f);
			if(dr<.01f-fishs.get(i).getKind()*.0016f && fishs.get(i).getLifeTime()>.5f)		fishs.get(i).setDirect(d);
			if(sr<.018f-fishs.get(i).getKind()*.0018f)		fishs.get(i).changeSpeed();
			fishs.get(i).update(delta);
			if(!fishs.get(i).isDying() && lasergun.isShoting() && distanceFromLaserLine(fishs.get(i).getX(), fishs.get(i).getY())<40){
				fishs.get(i).takeDamage(5000, false);
			}
			if(fishs.get(i).isDead()){
				killFish(fishs.get(i));
				fishs.remove(i);
			}
		}
	}
	
	public float distanceFromLaserLine(float x, float y){
		double a = Math.abs(laserline.x*x + laserline.y*y + laserline.z);
		double b = Math.sqrt(1 + laserline.y*laserline.y);
		return (float)(a/b);
	}
	
	private void draw(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		
		batcher.enableBlending();
		batcher.begin();
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		batcher.draw(Assets.scene[Settings.level%2], 0, 0);
		if(isSplitScene){
			batcher.draw(Assets.scene[scene] ,0, 0, splitScene, 480, 0, 0, splitScene, 480, false, false);
			batcher.draw(wave.getFrameImage(), splitScene-wave.getWidth()/2, 0, wave.getWidth(), 480);
		}
		
		for(FishObject fish : fishs){
			batcher.setColor(1, fish.getGBcolor(), fish.getGBcolor(), 1);
			batcher.draw(fish.getImage(), fish.getX()-fish.getOrigin().x, fish.getY()-fish.getOrigin().y, fish.getOrigin().x, fish.getOrigin().y, 
					fish.getWidth(), fish.getHeight(), 1, 1, fish.getRotation());
		}
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		else	batcher.setColor(1, 1, 1, 1);
		
		if(chosenItem>0){
			batcher.draw(Assets.effectCircle, touchpos.x-150, touchpos.y-150, 300, 300);
			batcher.draw(item, touchpos.x - item.getRegionWidth()/2, touchpos.y - item.getRegionHeight()/2);
		}
		drawCireffs();
		for(AnimationObject e: effs){
			batcher.draw(e.getFrameImage(), e.getX()-e.getWidth()/2, e.getY()-e.getHeight()/2);
		}
		for(FeededBread b: breads){
			batcher.draw(Assets.itemBread, b.getX()-Assets.itemBread.getRegionWidth()/2, b.getY()-Assets.itemBread.getRegionHeight()/2, 
					Assets.itemBread.getRegionWidth()/2, Assets.itemBread.getRegionHeight()/2, 
					Assets.itemBread.getRegionWidth(), Assets.itemBread.getRegionHeight(), b.getRate(), b.getRate(), b.getRota());
		}
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		else	batcher.setColor(1, 1, 1, 1);
		batcher.draw(Assets.content, 0, 0);
		batcher.draw(Assets.table, 346, 0);
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		for(BulletObject b : bullets){
			batcher.setColor(1, 1, 1, b.getAcolor());
			batcher.draw(b.getImage(), b.getX()-b.getOrigin().x, b.getY()-b.getOrigin().y, b.getOrigin().x, b.getOrigin().y, 
					b.getWidth(), b.getHeight(), 1, 1, b.getRotation());
//			if(b.getDamageRegion()!=null){
//				batcher.draw(Assets.btHelp, b.getDamageRegion().x, b.getDamageRegion().y, 10, 10);
//				batcher.draw(Assets.btHelp, b.getDamageRegion().x, b.getDamageRegion().y+b.getDamageRegion().height, 10, 10);
//				batcher.draw(Assets.btHelp, b.getDamageRegion().x+b.getDamageRegion().width, b.getDamageRegion().y, 10, 10);
//				batcher.draw(Assets.btHelp, b.getDamageRegion().x+b.getDamageRegion().width, b.getDamageRegion().y+b.getDamageRegion().height, 10, 10);
//			}
		}
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		else	batcher.setColor(1, 1, 1, 1);
		for(TwinkyStar s: stars){
			batcher.draw(s.getImage(), s.getX()-s.getWidth()*s.getScale()/2, s.getY()-s.getHeight()*s.getScale()/2, 
					s.getOrigin().x*s.getScale(), s.getOrigin().y*s.getScale(), s.getWidth()*s.getScale(), s.getHeight()*s.getScale(), 1, 1, s.getRotation());
		}
		for(CoinObject c : coins){
			batcher.draw(c.getImage(), c.getX()-c.getOrigin().x, c.getY()-c.getOrigin().y);
		}
//		Assets.next.flip(true, false);
//		Assets.nextPrs.flip(true, false);
		if(!right)
			batcher.draw(Assets.next, 455, -5, 54, 54);
		else batcher.draw(Assets.nextPrs, 455, -5, 54, 54);
		if(!left)
			batcher.draw(Assets.prev, 290, -5, 54, 54);
		else batcher.draw(Assets.prevPrs, 290, -5, 54, 54);
		if(lasergun.isActived()){
			lasergun.draw(batcher, gunbonusY);
		}
		else{
			batcher.draw(Assets.gunhead[gun.getPower().getKind()-1], gun.getX()-Assets.gunhead[gun.getPower().getKind()-1].getRegionWidth()/2, 
					gun.getY()+gun.getHeight()*gun.getHeadToGunRate()+gunbonusY, Assets.gunhead[gun.getPower().getKind()-1].getRegionWidth()/2, 
					-gun.getHeight()*gun.getHeadToGunRate(), Assets.gunhead[gun.getPower().getKind()-1].getRegionWidth(), 
					Assets.gunhead[gun.getPower().getKind()-1].getRegionHeight(), 1, 1, gun.getRotation());
			batcher.draw(gun.getImage(), gun.getX()-gun.getOrigin().x+gun.getBonusX(), gun.getY()-gun.getOrigin().y+gun.getBonusY()+gunbonusY, 
					gun.getOrigin().x, gun.getOrigin().y, gun.getWidth(), gun.getHeight(), 1, 1, gun.getRotation());
		}
		batcher.draw(Assets.btnEquip, btn_equip.x, btn_equip.y);
		
		batcher.draw(Assets.bubble, btn_bomb.x, btn_bomb.y, 70, 70);
		batcher.draw(Assets.bubble, btn_bread.x, btn_bread.y, 70, 70);
		batcher.draw(Assets.bubble, btn_shock.x, btn_shock.y, 70, 70);
		batcher.draw(Assets.itemBomb, btn_bomb.x+7, btn_bomb.y+2);
		batcher.draw(Assets.itemBread, btn_bread.x+7, btn_bread.y+2);
		batcher.draw(Assets.itemShock, btn_shock.x+7, btn_shock.y+2);
		Assets.fontItem.draw(batcher, Settings.bombNumb+"", btn_bomb.x+50, btn_bomb.y+20);
		Assets.fontItem.draw(batcher, Settings.breadNumb+"", btn_bread.x+50, btn_bread.y+20);
		Assets.fontItem.draw(batcher, Settings.shockNumb+"", btn_shock.x+50, btn_shock.y+20);
		
		batcher.draw(Assets.contentCoin, 10, 3);
		Assets.fontSmallMoney.draw(batcher, formatNumber((int)coinDraw, 9), 45, 30);
		Assets.fontCountTime.draw(batcher, ""+(int)timeAddCoin, 172, 30);
		batcher.draw(Assets.contentPower, 565, 3);
		batcher.draw(Assets.power ,571, 8, 98*Settings.power/100, 14, 0, 0, (int)(98*Settings.power/100), 14, false, false);
		batcher.draw(Assets.contentShell, 685, 3);
		Assets.fontSmallMoney.draw(batcher, formatNumber(Settings.shell, 6), 717, 27);
		batcher.draw(Assets.contentLevel, 684, 423);
		batcher.draw(Assets.circleLevel, 751, 433);
		batcher.draw(Assets.expball.getTexture(), 753, 435, 41, 41*Settings.exp/Settings.requiredExp, 683, 32, 41, (int)(41*Settings.exp/Settings.requiredExp), false, true);
		Assets.fontLevel.drawWrapped(batcher, ""+Settings.level, 757, 465, 30, HAlignment.CENTER);
		for(CoinStage cs: coinstage){
			batcher.setColor(1, 1, 1, cs.aColor);
			Assets.fontBigMoney.draw(batcher, "x"+cs.numb, cs.x-25, cs.y+30);
		}
		if(Settings.ispause)	batcher.setColor(.5f, .5f, .5f, 1);
		else	batcher.setColor(1, 1, 1, 1);
		if(isSplitScene && !levelUp_congrate.isTimeUp()){
			batcher.draw(levelUp_congrate.getImage(), levelUp_congrate.x, levelUp_congrate.y);
		}
		for(CoinStage ss: shellstage){
			batcher.setColor(1, 1, 1, ss.aColor);
			Assets.fontShell.draw(batcher, "x"+ss.numb, ss.x-45, ss.y+30);
			batcher.draw(Assets.bigShell, ss.x+40, ss.y-65);
		}
		batcher.setColor(1, 1, 1, 1);
		
		if(Settings.ispause){
			batcher.draw(Assets.pause_bg, 183, 86);
			batcher.draw(btnSound, 500, 250);
			batcher.draw(btnContinue, 300, 220);
			batcher.draw(btnMenu, 300, 150);
		}
		
		batcher.end();
	}
	
	public void drawCireffs(){
		for(AnimationObject ce: cireffs){
			if(ce.isPrepare())		continue;
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 0);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 45);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 90);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 135);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 180);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 225);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 270);
			batcher.draw(ce.getFrameImage(), ce.getX()-ce.getFrameImage().getRegionWidth()/2, ce.getY(), ce.getFrameImage().getRegionWidth()/2, 0, 
					ce.getFrameImage().getRegionWidth(), ce.getFrameImage().getRegionHeight(), 1, 1, 315);
		}
	}
	
	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}
	
	public void shotBullet(int x, int y){
		if(gunin || gunout)		return;
		if(lasergun.isActived() && lasergun.isWaiting()){
			Assets.playSound(Assets.sound_lasershot);
			lasergun.shot(x, y);
			laserline = new Vector3(y-10, 400-x, 0);
			int temp = (int)(-400*laserline.x - 10*laserline.y);
			laserline.y /= laserline.x;
			laserline.z = temp/laserline.x;
			laserline.x = 1;
			return;
		}
		if(Settings.coin<gun.getCost())		return;
		Assets.playSound(Assets.sound_shot);
		float tx = x-gun.getX();
		float ty = y-gun.getY();
		gun.load();
		gun.setRotation((float)-Math.toDegrees(Math.atan(tx/ty)));
		statenet = stateGun/3;
		bullets.add(new BulletObject(Assets.bullet[statenet], gun.getX(), gun.getY(), 
						gun.getRotation(), 10, -gun.getHeight()*gun.getHeadToGunRate(), new Vector2(x, y), gun.getSpeed(), gun.getDamage()));
		bullets.get(bullets.size-1).fixVelocity(480);
		Settings.coin -= gun.getCost();
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		guiCam.unproject(touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		x = (int)touchpos.x;
		y = (int)touchpos.y;
		
		if(Settings.ispause && OverlapTester.pointInRectangle(btn_sound, x, y)){
			Assets.playSound(Assets.sound_button);
			if(Settings.soundEnabled){
				btnSound = Assets.pause_sound_off;
			}else{
				btnSound = Assets.pause_sound_on;
			}
			Settings.soundEnabled = !Settings.soundEnabled;
			return false;
		}
		if(Settings.ispause && OverlapTester.pointInRectangle(btn_continue, x, y)){
			Assets.playSound(Assets.sound_button);
			btnContinue = Assets.pause_continue_push;
			return false;
		}
		if(Settings.ispause && OverlapTester.pointInRectangle(btn_menu, x, y)){
			Assets.playSound(Assets.sound_button);
			btnMenu = Assets.pause_menu_push;
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_equip, x, y)){
			Assets.playSound(Assets.sound_button);
			game.shopscreen.fromGame();
			game.setScreen(game.shopscreen);
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_bomb, x, y)){
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_bread, x, y)){
			return false;
		}
		if(OverlapTester.pointInRectangle(btn_shock, x, y)){
			return false;
		}
		if(y>40)	shotBullet(x, y);
		if (OverlapTester.pointInRectangle(leftBound, x ,y)) {
			Assets.playSound(Assets.sound_changeGun);
			left = true;
		} else if (OverlapTester.pointInRectangle(rightBound, x ,y)) {
			Assets.playSound(Assets.sound_changeGun);
			right = true;
		}
		//Gdx.app.log(""+x, ""+y);
		return false;
	}

	@Override
	public boolean touchUp(int x, int y, int pointer, int button) {
		guiCam.unproject(touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		x = (int)touchpos.x;
		y = (int)touchpos.y;
		if(Settings.ispause && OverlapTester.pointInRectangle(btn_continue, x, y)){
			btnContinue = Assets.pause_continue_normal;
			Settings.ispause = false;
			return false;
		}
		if(Settings.ispause && OverlapTester.pointInRectangle(btn_menu, x, y)){
			btnMenu = Assets.pause_menu_normal;
			Settings.ispause = false;
			Settings.save();
			game.setScreen(game.menuscreen);
			return false;
		}
		if(left){
			if(stateGun == 0) stateGun = 8;
			else stateGun--;
			while(GunManager.gun[stateGun].isLocked())	stateGun --;
			left = false;
			gunout = true;
		}
		if(right){
			stateGun++;
			while(stateGun<9 && GunManager.gun[stateGun].isLocked())	stateGun ++;
			if(stateGun > 8) stateGun = 0;
			right = false;
			gunout = true;
		}
		if(chosenItem>0){
			if(chosenItem==1 && !OverlapTester.pointInRectangle(btn_bomb, x, y)){
				effs.add(new AnimationObject(Assets.bombeffect, x, y, .15f, 0, 0, true));
				bombExplosion(x, y);
				Settings.bombNumb --;
				Assets.playSound(Assets.sound_Bomb);
			}
			else if(chosenItem==2 && !OverlapTester.pointInRectangle(btn_bread, x, y)){
				effs.add(new AnimationObject(Assets.breadeff, x, y, .15f, 10, 0, false));
				breads.add(new FeededBread(x, y, 300, 300, 10));
				Settings.breadNumb --;
				Assets.playSound(Assets.sound_Bread);
			}
			else if(chosenItem==3 && !OverlapTester.pointInRectangle(btn_shock, x, y)){
				effs.add(new AnimationObject(Assets.shockeff1, x, y, .15f, 0, 0, true));
				cireffs.add(new AnimationObject(Assets.shockeff2, x, y, .15f, 0, .3f, true));
				shockExplosion(x, y);
				Settings.shockNumb --;
				Assets.playSound(Assets.sound_Shock);
			}
			chosenItem = -1;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		guiCam.unproject(touchpos.set(Gdx.input.getX(), Gdx.input.getY(), 0));
		x = (int)touchpos.x;
		y = (int)touchpos.y;
		if(chosenItem<0 && Settings.bombNumb>0 && OverlapTester.pointInRectangle(btn_bomb, x, y)){
			chosenItem = 1;
			item = Assets.itemBomb;
			return false;
		}
		if(chosenItem<0 && Settings.breadNumb>0 &&OverlapTester.pointInRectangle(btn_bread, x, y)){
			chosenItem = 2;
			item = Assets.itemBread;
			return false;
		}
		if(chosenItem<0 && Settings.shockNumb>0 &&OverlapTester.pointInRectangle(btn_shock, x, y)){
			chosenItem = 3;
			item = Assets.itemShock;
			return false;
		}
		return false;
	}
	
	public String formatNumber(int numb, int count){
		String str = "";
		String s = ""+numb;
		if(s.length() >= count) return s;
		else{
			int sub = count - s.length();
			for(int i = 0; i < sub; i++){
				str = str + "0";
			}
			str = str + s;
		}
		return str;
	}
	
	public void addCoin(int coinAdd){
		Settings.coin += coinAdd;
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		gun = new GunObject(GunManager.gun[stateGun], 400, 10, 0);
		lasergun = new LaserGun(400, 10, 50, 45);
		wave = new AnimationObject(Assets.wave, .2f, 0, 0, 5);
		levelUp_congrate = new CoinStage(Assets.levelUp_congrate, 190, 200, 15, .5f);
		Gdx.input.setInputProcessor(this);
		left = false;
		right = false;
		chosenItem = -1;
		splitScene = 800;
		scene = Settings.level%2;
		levelUp_congrate.upTheTime();
		//get coin, get shell
		coinDraw = Settings.coin;
		if(Settings.soundEnabled){
			btnSound = Assets.pause_sound_on;
		}else{
			btnSound = Assets.pause_sound_off;
		}
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.BACKSPACE)	Settings.ispause = true;
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
}
