package com.dvc.vn.fishhunter.ultility;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class SaveGame {
	public static Preferences prefs = Gdx.app.getPreferences("FishHunterPreference");
	
	public SaveGame(){
		
	}
	
	public static void putInt(String SharedPreferenceName, int a){
		prefs.putInteger(SharedPreferenceName, a);
		prefs.flush();
	}
	
	public static int getInt(String SharedPreferenceName, int b){
		int a = prefs.getInteger(SharedPreferenceName, b);
		return a;
	}
	
	public static void putFloat(String SharedPreferenceName, float a){
		prefs.putFloat(SharedPreferenceName, a);
		prefs.flush();
	}
	
	public static float getFloat(String SharedPreferenceName, float b){
		float a = prefs.getFloat(SharedPreferenceName, b);
		return a;
	}
	
	public static void putBoolean(String SharedPreferenceName, boolean a){
		prefs.putBoolean(SharedPreferenceName, a);
		prefs.flush();
	}
	
	public static boolean getBoolean(String SharedPreferenceName, boolean b){
		boolean a = prefs.getBoolean(SharedPreferenceName, b);
		return a;
	}
}

