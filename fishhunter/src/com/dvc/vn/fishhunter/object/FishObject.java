package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class FishObject extends GameObject {
	
	private int hp;
	private int kind;
	private float time;
	private float lifetime;
	private float speed;
	private float speedInTime;
	private float targetRota;
	private float rotachanger;
	private float dyingtime;
	private float timetodead;
	private float timetoshock;
	private float shocktime;
	private float GBcolor;
	private Vector2 velocity;
	private boolean isliving;
	private boolean isDead;
	private boolean isDying;
	private boolean isshocked;
	private boolean iseating;
	private boolean isshotdead;
	private boolean isdaming;
	private AnimationObject animation;
	private FishInfo info;
	private FeededBread breadFound;
	
	public FishObject(FishInfo info, float x, float y, float rotation, float bonusHp) {
		super(info.animation.getFrameImage(), x, y, rotation, info.originX, info.originY);
		velocity = new Vector2(0, 0);
		isliving = true;
		this.speed = info.speed;
		this.kind = info.kind;
		this.hp = (int)(info.hp*(1+bonusHp));
		time = 0;
		speedInTime = 0;
		lifetime = 0;
		GBcolor = 1;
		animation = new AnimationObject(info.animation);
		this.info = info;
	}
	
	public void update(float delta){
		if(isliving && !isshocked){
			lifetime += delta;
			if(velocity.x!=0 && velocity.y!=0){
				x += velocity.x*delta*speed;
				y += velocity.y*delta*speed;
			}
			if(speedInTime>0){
				time += delta/2;
				if(time>speedInTime){
					speed = info.speed;
					speedInTime = 0;
					time = 0;
				}
			}
			changePosByRota(delta);
			if(rotachanger!=0){
				if(iseating)	rotation += rotachanger*delta;
				else	rotation += rotachanger*delta/5;
				if(Math.abs(rotation-targetRota)<1){
					rotation = targetRota;
					rotachanger = 0;
				}
			}
		}
		if(isshocked){
			timetoshock += delta;
			if(timetoshock>shocktime){
				isshocked = false;
			}
		}
		if(iseating){
			changeRotaToFindBread();
			if(breadFound.isCompleted()){
				iseating = false;
			}
		}
		if(isDying){
			timetodead += delta;
			if(timetodead>dyingtime){
				isDying = false;
				isDead = true;
			}
		}
		if(isdaming){
			if(GBcolor>0)	GBcolor -= delta*5;
			else{
				GBcolor = 0;
				isdaming = false;
			}
		}
		else{
			if(GBcolor<1)	GBcolor += delta*5;
			else	GBcolor = 1;
		}
		animation.update(delta);
	}
	
	public boolean reachBread(){
		return Math.abs(x-breadFound.getX())<100 && Math.abs(y-breadFound.getY())<100;
	}
	
	public void changeRotaToFindBread(){
		if(breadFound==null || breadFound.isCompleted())		return;
		float r = (float)Math.toDegrees(Math.atan2((breadFound.getY()-y),(breadFound.getX()-x)));
		rotachanger = r - rotation;
		if(rotachanger>0)	rotachanger -= 360;
	}
	
	public void inShockState(float shocktime){
		isshocked = true;
		timetoshock = 0;
		this.shocktime = shocktime;
	}
	
	public float getGBcolor(){
		return GBcolor;
	}
	
	public boolean isEating(){
		return iseating;
	}
	
	public void setFoundBread(FeededBread bread){
		if(iseating || !isliving || isshocked)		return;
		breadFound = bread;
		iseating = true;
		speedInTime = .01f;
	}
	
	public boolean isDying(){
		return isDying;
	}
	
	public void setDyingState(){
		animation.setRunFrame(info.dyingStartFrame, info.dyingEndFrame, info.dyingFrameTime);
		this.dyingtime = info.dyingTime;
		timetodead = 0;
		isliving = false;
		isDying = true;
	}
	
	public Vector2 fixVelocity(float range, Vector2 v){
		long r2 = (long)range*(long)range;
		float x2 = v.x*v.x;
		float y2 = v.y*v.y;
		int kx = 1, ky = 1;
		if(v.x<0)	kx = -1;
		if(v.y<0)	ky = -1;
		v.x = (float)Math.sqrt(r2*x2/(x2 + y2))*kx;
		v.y = (float)Math.sqrt(r2*y2/(x2 + y2))*ky;
		return v;
	}
	
	public int getKind(){
		return kind;
	}
	
	public void changePosByRota(float delta){
		if(rotation>180)	rotation -= 360;
		if(rotation<-180)	rotation += 360;
		float vx = 0, vy = 0;
		if(rotation==0){
			vx = 1;
		}else if(rotation==90){
			vy = 1;
		}else if(rotation==180 || rotation==-180){
			vx = -1;
		}else if(rotation==-90){
			vy = -1;
		}else{
			if(Math.abs(rotation)<90){
				vx = 1;
				vy = (float)Math.tan(Math.toRadians(rotation));
			}else{
				vx = -1;
				vy = -(float)Math.tan(Math.toRadians(rotation));
			}
		}
		Vector2 v = fixVelocity(240, new Vector2(vx, vy));
		x += v.x*delta*speed;
		y += v.y*delta*speed;
	}
	
	public float getLifeTime(){
		return lifetime;
	}
	
	public void setDirect(float r){
		targetRota = r;
		rotachanger = r - rotation;
	}
	
	public void setAnimationImages(TextureRegion[] t, int startFrame, int endFrame, float frametime){
		animation.setFramesImage(t, startFrame, endFrame, frametime);
	}
	
	public TextureRegion getImage(){
		TextureRegion t = new TextureRegion(animation.getFrameImage());
		if(Math.abs(rotation)>90){
			t.flip(false, true);
		}
		return t;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}

	public float getSpeed() {
		return speed;
	}
	
	public boolean isShotDead(){
		return isshotdead;
	}
	
	public void takeDamage(int damage, boolean isshot){
		isdaming = true;
		hp -= damage;
		if(hp<=0){
			isshotdead = isshot;
			setDyingState();
		}
	}
	
	public boolean isOutOfHealth(){
		return hp<=0;
	}
	
	public void changeSpeed() {
		if(isDying)		return;
		speed = info.boostSpeed;
		speedInTime = info.boostTime;
		animation.changeFrameTime(info.aniBoostFrameTime, info.boostTime);
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}
	
	public int getCoins(){
		return info.coin;
	}

}
