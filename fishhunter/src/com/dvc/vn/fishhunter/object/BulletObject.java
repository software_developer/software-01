package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class BulletObject extends GameObject{
	private int damage;
	private float speed;
	private float scale;
	private float acolor;
	private float speedScale;
	private Vector2 target;
	private Vector2 velocity;
	private boolean isshooting;
	private boolean isexplosing;
	private boolean isdisappearing;
	private boolean isdestroyed;
	private Rectangle damageRegion;

	public BulletObject(TextureRegion image, float x, float y, float rotation,
			float originX, float originY, Vector2 target, float speed, int damage) {
		super(image, x, y, rotation, originX, originY);
		this.target = target;
		this.speed = speed;
		this.damage = damage;
		this.velocity = new Vector2(target.x - x, target.y - y);
		isshooting = true;
		scale = 1;
		acolor = 1;
		speedScale = 1;
	}
	
	public BulletObject(TextureRegion image, float x, float y, float rotation,
			float originX, float originY, Vector2 target, float speed) {
		super(image, x, y, rotation, originX, originY);
		this.target = target;
		this.speed = speed;
		this.velocity = new Vector2(target.x - x, target.y - y);
		isshooting = true;
		scale = 1;
		acolor = 1;
		speedScale = 1;
	}

	public BulletObject(TextureRegion image, float x, float y) {
		super(image, x, y);
		velocity = new Vector2(0, 0);
		speed = 1;
		isshooting = true;
		scale = 1;
		acolor = 1;
		speedScale = 1;
	}
	
	public void setTarget(Vector2 t){
		target.x = t.x;
		target.y = t.y;
		velocity.x = x - t.x;
		velocity.y = y - t.y;
	}
	
	public void setVelocity(float vx, float vy){
		velocity.x = vx;
		velocity.y = vy;
	}
	
	public void fixVelocity(float range){
		long r2 = (long)range*(long)range;
		long x2 = (long)velocity.x*(long)velocity.x;
		long y2 = (long)velocity.y*(long)velocity.y;
		int k = 1;
		if(velocity.x<0)	k = -1;
		velocity.x = (float)Math.sqrt(r2*x2/(x2 + y2))*k;
		velocity.y = (float)Math.sqrt(r2*y2/(x2 + y2));
	}

	public float getWidth(){
		return super.getWidth()*scale;
	}
	
	public float getHeight(){
		return super.getHeight()*scale;
	}
	
	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public float getSpeedScale() {
		return speedScale;
	}

	public void setSpeedScale(float speedScale) {
		this.speedScale = speedScale;
	}

	public Vector2 getVelocity() {
		return velocity;
	}
	
	public float getAcolor(){
		if(acolor>1)	return 1;
		if(acolor<0)	return 0;
		return acolor;
	}
	
	public boolean isIsshooting() {
		return isshooting;
	}

	public void setIsshooting(boolean isshooting) {
		this.isshooting = isshooting;
	}
	
	public Rectangle getDamageRegion(){
		return damageRegion;
	}
	
	public void explose(){
		isshooting = false;
		isexplosing = true;
		damageRegion = new Rectangle(getX()-super.getWidth()/3, 
						getY()-super.getHeight()/3, super.getWidth()*2/3, super.getHeight()*2/3);
	}
	
	public Vector2 getTarget(){
		return target;
	}
	
	public boolean reachedTarget(){
		float dx = Math.abs(x - target.x);
		float dy = Math.abs(y - target.y);
		return Math.sqrt(dx*dx+dy*dy)<10 && isshooting;
	}
	
	public boolean isDestroyed(){
		return isdestroyed;
	}
	
	public void update(float delta){
		if(isshooting){
			if(velocity.x==0 && velocity.y==0)		return;
			x += velocity.x*delta*speed;
			y += velocity.y*delta*speed;
		}
		else if(isexplosing){
			scale += delta*speedScale;
			if(scale>1.1f){
				isexplosing = false;
				isdisappearing = true;
			}
		}
		else if(isdisappearing){
			if(scale>1)		scale -= delta*speedScale;
			else{
				if(acolor>0)	acolor -= delta;
				else{
					isdisappearing = false;
					isdestroyed = true;
				}
			}
		}
		origin.x = getWidth()/2;
		origin.y = getHeight()/2;
	}
	
}
