package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class CoinObject extends GameObject{
	
	private float speed;
	private Vector2 target;
	private Vector2 velocity;
	AnimationObject animation;
	
	public CoinObject(TextureRegion[] t, float x, float y, float rotation,
			float originX, float originY, Vector2 target, float speed) {
		super(t[0], x, y, rotation, originX, originY);
		this.target = target;
		this.speed = speed;
		this.velocity = new Vector2(target.x - x, target.y - y);
		animation = new AnimationObject(t, .07f, 0, 0, t.length-1);
	}
	
	public void update(float delta){
		if(velocity.x==0 && velocity.y==0)		return;
		x += velocity.x*delta*speed;
		y += velocity.y*delta*speed;
		animation.update(delta);
	}
	
	public boolean reachedTarget(){
		float dx = Math.abs(x - target.x);
		float dy = Math.abs(y - target.y);
		return Math.sqrt(dx*dx+dy*dy)<2;
	}
	
	public TextureRegion getImage(){
		return animation.getFrameImage();
	}
}
