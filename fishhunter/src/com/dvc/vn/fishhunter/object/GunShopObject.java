package com.dvc.vn.fishhunter.object;

public class GunShopObject {
	private int state;
	private int kindGun;
	private int levelPower;
	private int levelSpeed;
	private int costPower;
	private int costSpeed;
	private int costUnlock;
	
	public GunShopObject(int kindGun, int state, int levelPower, int levelSpeed){
		this.state = state;
		this.kindGun = kindGun;
		this.levelPower = levelPower;
		this.levelSpeed = levelSpeed;
		setCost();
	}
	
	public void setCost(){
		costPower = 300*(kindGun+1)+(levelPower-1)*300*(kindGun+1)/2;
		costSpeed = 300*(kindGun+1)+(levelSpeed-1)*300*(kindGun+1)/2;
		if(kindGun == 7) costUnlock = 2400;
		if(kindGun == 8) costUnlock = 6000;
	}
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}
	
	public int getKindGun() {
		return kindGun;
	}

	public void setKindGun(int kindGun) {
		this.kindGun = kindGun;
	}

	public int getLevelPower() {
		return levelPower;
	}
	
	public void setLevelPower(int levelPower) {
		this.levelPower = levelPower;
		setCost();
	}
	
	public int getLevelSpeed() {
		return levelSpeed;
	}
	
	public void setLevelSpeed(int levelSpeed) {
		this.levelSpeed = levelSpeed;
		setCost();
	}
	
	public int getCostPower() {
		return costPower;
	}
	
	public void setCostPower(int costPower) {
		this.costPower = costPower;
	}
	
	public int getCostSpeed() {
		return costSpeed;
	}
	
	public void setCostSpeed(int costSpeed) {
		this.costSpeed = costSpeed;
	}

	public int getCostUnlock() {
		return costUnlock;
	}

	public void setCostUnlock(int costUnlock) {
		this.costUnlock = costUnlock;
	}
	
}
