package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class ButtonMenuObject extends GameObject {

	private float time;
	private float time_start;
	private boolean isDraw;
	private boolean direction;
	private float scale;
	private Rectangle rec;
	private float y1,y2;
	
	public ButtonMenuObject(TextureRegion image, float x, float y, float time_start) {
		super(image, x, y);
		this.time_start = time_start;
		isDraw = false;
		time = 0;
		scale = 0.1f;
		direction = false;
		y1 = y + image.getRegionWidth()/10;
		y2 = y - image.getRegionWidth()/10;
		rec = new Rectangle(x,y,image.getRegionWidth(),image.getRegionHeight());
	}
	
	public void update(float delta){
		if(!isDraw)
			time+=delta;
		if(time >= time_start){
			isDraw = true;
			if(scale <= 0.9)
				scale += delta*2;
			else {
				scale = 1;
				if(!direction){
					if(y > y2) y -= delta*15;
					else{
						y = y2;
						direction = true;
					}
				} else{
					if(y < y1) y += delta*15;
					else{
						y = y1;
						direction = false;
					}
				}
				rec = new Rectangle(x-getWidth()/2,y-getHeight()/2,image.getRegionWidth(),image.getRegionHeight());
			}
		}
		
	}

	public Rectangle getRec() {
		return rec;
	}

	public float getWidth(){
		return super.getWidth()*scale;
	}
	
	public float getHeight(){
		return super.getHeight()*scale;
	}
	
	public boolean isDraw() {
		return isDraw;
	}
}
