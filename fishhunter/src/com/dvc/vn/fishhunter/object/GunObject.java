package com.dvc.vn.fishhunter.object;

public class GunObject extends GameObject{

	private int damage;
	private float speed;
	private float headtogun;
	private float bonusY;
	private GunPower gpower;
	private boolean isloadbull = false;		//bắn xong chuyển sang chế độ nạp đạn - thụt vào
	
	public GunObject(GunPower gunpower, float x, float y, float rotation) {
		super(gunpower.image, x, y, rotation, gunpower.originX, gunpower.originY);
		gpower = gunpower;
		headtogun = gpower.headtogun;
		damage = gunpower.getDamage();
		speed = gunpower.getSpeed();
	}
	
	public void update(float delta){
		//nếu đang nạp đạn, đầu súng thụt vào; Thụt đến tỷ lệ .5f thì bắt đầu đẩy ra đến tỷ lệ gốc là .61f
		if(isloadbull && headtogun>.5f){
			bonusY -= delta*20;
			headtogun -= delta;
			if(headtogun<=.5f)	isloadbull = false;
		}
		else if(headtogun<.61f){
			bonusY += delta*20;
			headtogun += delta;
			if(headtogun>.61f){
				headtogun = .61f;
				bonusY = 0;
			}
		}
	}
	
	public void updateFromShopping(){
		damage = gpower.getDamage();
		speed = gpower.getSpeed();
	}
	
	public float getBonusY(){
		return (float)Math.cos(Math.toRadians(rotation))*bonusY;
	}
	
	public float getBonusX(){
		return	(float)-Math.sin(Math.toRadians(rotation))*bonusY;
	}
	
	public void setGunPower(GunPower gp){
		gpower = gp;
		headtogun = gp.headtogun;
		image = gp.image;
		origin.x = gp.originX;
		origin.y = gp.originY;
		damage = gpower.getDamage();
		speed = gpower.getSpeed();
	}
	
	public void load(){
		isloadbull = true;
	}

	public GunPower getPower(){
		return gpower;
	}

	public int getCost(){
		return gpower.cost;
	}
	
	public float getHeadToGunRate(){
		return headtogun;
	}
	
	public int getDamage(){
		return damage;
	}
	
	public float getSpeed(){
		return speed;
	}
	
}
