package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.dvc.vn.fishhunter.Assets;


public class LaserGun {
	private float x, y;
	private float oriX, oriY;
	private float rotation;
	private float time;
	private float shottime;
	private boolean isactived;
	private boolean iswaiting;
	private boolean isshoting;
	private boolean isCompleted;
	private AnimationObject shot, light;
	private AnimationObject[] eff;
	
	public LaserGun(float x, float y, float oriX, float oriY){
		this.x = x;
		this.y = y;
		this.oriX = oriX;
		this.oriY = oriY;
		rotation = 0;
		time = 0;
		shottime = 3;
		shot = new AnimationObject(Assets.lasershot, x, y, .15f, 0, 0, false);
		light = new AnimationObject(Assets.laserlight, x, y, .15f, 0, 0, false);
		eff = new AnimationObject[3];
		eff[0] = new AnimationObject(Assets.lasereff1, x, y, .2f, 0, 0, false);
		eff[1] = new AnimationObject(Assets.lasereff2, x, y, .2f, 0, 0, false);
		eff[2] = new AnimationObject(Assets.lasereff3, x, y, .2f, 0, 0, false);
	}
	
	public void update(float delta){
		if(!isactived)		return;
		if(iswaiting){
			for(int i=0;i<eff.length;i++){
				eff[i].update(delta);
			}
		}
		if(isshoting){
			time += delta;
			shot.update(delta);
			light.update(delta);
			if(time>shottime){
				isshoting = false;
				isCompleted = true;
				isactived = false;
			}
		}
	}
	
	public void draw(SpriteBatch batcher, float bonusY){
		batcher.draw(Assets.lasergun, x-oriX, y-oriY+bonusY, oriX, oriY, 
				Assets.lasergun.getRegionWidth(), Assets.lasergun.getRegionHeight(), 1, 1, rotation);
		if(iswaiting){
			if(eff[0].getFrame()<5)		batcher.draw(eff[0].getFrameImage(), x-eff[0].getFrameImage().getRegionWidth()/2, y+10+bonusY);
			else	batcher.draw(eff[0].getFrameImage(), x-eff[0].getFrameImage().getRegionWidth()/2, y+60+bonusY);
			batcher.draw(eff[1].getFrameImage(), x-eff[1].getFrameImage().getRegionWidth()/2, y+20+bonusY);
			batcher.draw(eff[2].getFrameImage(), x-eff[2].getFrameImage().getRegionWidth()/2, y+20+bonusY);
		}
		if(isshoting){
			batcher.draw(shot.getFrameImage(), x-shot.getFrameImage().getRegionWidth()/2, y+40, 80, -40,
					shot.getFrameImage().getRegionWidth(), shot.getFrameImage().getRegionHeight(), 1, 1, rotation);
			batcher.draw(light.getFrameImage(), x-light.getFrameImage().getRegionWidth()/2, y+200, light.getFrameImage().getRegionWidth()/2,
					-200, light.getFrameImage().getRegionWidth(), 500, 1, 1, rotation);
		}
	}
	
	public boolean isActived(){
		return isactived;
	}
	
	public void active(){
		isactived = true;
		iswaiting = true;
	}
	
	public void shot(float x, float y){
		iswaiting = false;
		isshoting = true;
		rotation = (float)-Math.toDegrees(Math.atan((x-this.x)/(y-this.y)));
	}
	
	public boolean isShoting(){
		return isshoting;
	}
	
	public boolean isWaiting(){
		return iswaiting;
	}
	
	public boolean isCompleted(){
		return isCompleted;
	}
	
	public void reset(){
		active();
		rotation = 0;
		time = 0;
	}
}
