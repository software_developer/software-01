package com.dvc.vn.fishhunter.object;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.dvc.vn.fishhunter.Assets;

public class TrunkObject extends GameObject {

	private int coin;
	private int state;
	private float scale;
	private float time;
	private float CONS_X1 = 165;
	private float CONS_X2 = 370;
	private float CONS_Y2 = 65;
	private float CONS_Y1 = 200;
	private int slot;
	private boolean isMovingX;
	private boolean isMovingY;
	private boolean huongX;
	private boolean huongY;
	private Rectangle rec;
	private float aColor;
	
	public TrunkObject(TextureRegion image, float x, float y, int coin, int slot) {
		super(image, x, y);
		this.coin = coin;
		this.slot = slot;
		state = 1;
		scale = 0.7f;
		isMovingX = false;
		isMovingY = false;
		setPosition(slot);
		rec = new Rectangle(getX(),getY(),getWidth(),getHeight());
		aColor = 1;
	}
	
	public void update(float delta){
		time += delta;
		if(time >= 1 && state == 1){
			setImage(Assets.trunk);
			state = 2;
		} 
		if(state == 2){
			if(isMovingX || isMovingY){
				if(huongX){
					if(getOrigin().x > getX()){
						setX(getX() + 2050*delta);
					}
					else {
						setX(getOrigin().x);
						isMovingX = false;
					}
				} else{
					if(getOrigin().x < getX()){
						setX(getX() - 2050*delta);
					}
					else {
						setX(getOrigin().x);
						isMovingX = false;
					}
				}
				if(huongY){
					if(getOrigin().y > getY()){
						setY(getY() + 1350*delta);
					}
					else {
						setY(getOrigin().y);
						isMovingY = false;
					}
				} else{
					if(getOrigin().y < getY()){
						setY(getY() - 1350*delta);
					}
					else {
						setY(getOrigin().y);
						isMovingY = false;
					}
				}
				if(!isMovingX && !isMovingY){ 
					setSlot();
				}
			}
		} else if(state == 3){
			if(aColor > 0.2)
				aColor -= delta*3;
			else aColor = 0;
		} else if(state == 4){
			scale += delta*0.6;
			if(isMovingX || isMovingY){
				if(huongX){
					if(getOrigin().x > getX()){
						setX(getX() + 206*delta);
					}
					else {
						setX(getOrigin().x);
						isMovingX = false;
					}
				} else{
					if(getOrigin().x < getX()){
						setX(getX() - 206*delta);
					}
					else {
						setX(getOrigin().x);
						isMovingX = false;
					}
				}
				if(huongY){
					if(getOrigin().y > getY()){
						setY(getY() + 136*delta);
					}
					else {
						setY(getOrigin().y);
						isMovingY = false;
					}
				} else{
					if(getOrigin().y < getY()){
						setY(getY() - 136*delta);
					}
					else {
						setY(getOrigin().y);
						isMovingY = false;
					}
				}
				if(!isMovingX && !isMovingY){ 
					state = 5;
					scale = 1;
				}
			}
		} else if(state == 5){
			if(coin == 600)
				setImage(Assets.trunkBigGold);
			else setImage(Assets.trunkGold);
		}
	}
	
	public void setSlot(){
		if(getX() == CONS_X1 && getY() == CONS_Y1) slot = 0;
		if(getX() == CONS_X2 && getY() == CONS_Y1) slot = 1;
		if(getX() == CONS_X1 && getY() == CONS_Y2) slot = 2;
		if(getX() == CONS_X2 && getY() == CONS_Y2) slot = 3;
		rec = new Rectangle(getX(),getY(),getWidth(),getHeight());
	}

	public Vector2 getSlot(int i){
		Vector2 v = null;
		switch (i) {
		case 0:
			v = new Vector2(CONS_X1,CONS_Y1);
			break;
		case 1:
			v = new Vector2(CONS_X2,CONS_Y1);
			break;
		case 2:
			v = new Vector2(CONS_X1,CONS_Y2);
			break;
		case 3:
			v = new Vector2(CONS_X2,CONS_Y2);
			break;
		default:
			break;
		}
		return v;
	}
	
	public void setPosition(int slot){
		switch (slot) {
		case 0:
			setX(CONS_X1);
			setY(CONS_Y1);
			break;
		case 1:
			setX(CONS_X2);
			setY(CONS_Y1);
			break;
		case 2:
			setX(CONS_X1);
			setY(CONS_Y2);
			break;
		case 3:
			setX(CONS_X2);
			setY(CONS_Y2);
			break;

		default:
			break;
		}
	}
	
	public void move(Vector2 vecto){
		setOrigin(vecto);
		if(this.x < vecto.x) huongX = true;
		else huongX = false;
		if(this.y < vecto.y) huongY = true;
		else huongY = false;
		isMovingX = true;
		isMovingY = true;
		
	}
	
	public float getaColor() {
		return aColor;
	}

	public void setaColor(float aColor) {
		this.aColor = aColor;
	}

	public int getCoin() {
		return coin;
	}

	public void setCoin(int coin) {
		this.coin = coin;
	}

	public Rectangle getRec() {
		return rec;
	}

	public void setRec(Rectangle rec) {
		this.rec = rec;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getSlot() {
		return slot;
	}

	public void setSlot(int slot) {
		this.slot = slot;
	}

	public float getWidth(){
		return super.getWidth()*scale;
	}
	
	public float getHeight(){
		return super.getHeight()*scale;
	}
	
}
