package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationObject {
	private TextureRegion[] frames;
	private int frame;
	private int startFrame;
	private int endFrame;
	private float x, y;
	private float rate;
	private float time;
	private float frameTime;
	private float newtime;
	private float intime;
	private float basicFrameTime;
	private float life;
	private float lifetime;
	private float pretime;
	private boolean onetime;
	private boolean isCompleted;
	
	
	public AnimationObject(TextureRegion[] frames, float frameTime, float lifetime, int startFrame, int endFrame){
		this.frames = frames;
		this.time = 0;
		this.frame = 0;
		this.rate = 1;
		this.frameTime = frameTime;
		this.basicFrameTime = frameTime;
		this.newtime = 0;
		this.intime = 0;
		this.startFrame = startFrame;
		this.endFrame = endFrame;
	}
	
	public AnimationObject(TextureRegion[] frames, float x, float y, float frameTime, float lifetime, float pretime, boolean onetime){
		this.x = x;
		this.y = y;
		this.rate = 1;
		this.time = 0;
		this.life = 0;
		this.frame = 0;
		this.pretime = pretime;
		this.startFrame = 0;
		this.endFrame = frames.length-1;
		this.frames = frames;
		this.frameTime = frameTime;
		this.lifetime = lifetime;
		this.onetime = onetime;
	}
	
	public AnimationObject(AnimationObject a){
		this.frames = a.frames;
		this.time = 0;
		this.frame = 0;
		this.rate = 1;
		this.frameTime = a.frameTime;
		this.basicFrameTime = a.frameTime;
		this.newtime = 0;
		this.intime = 0;
		this.startFrame = a.startFrame;
		this.endFrame = a.endFrame;
	}
	
	public void setStartFrame(int frame){
		this.startFrame = frame;
		this.frame = frame;
	}
	
	public void update(float delta){
		if(pretime>0){
			pretime -= delta;
			return;
		}
		if(lifetime>0){
			life += delta;
			if(life>=lifetime)		isCompleted = true;
		}
		time += delta;
		if(time>frameTime){
			if(frame<endFrame)		frame ++;
			else{
				if(!onetime)	frame = startFrame;
				else{
					frame = endFrame;
					isCompleted = true;
				}
			}
			time = 0;
		}
		if(intime>0){
			newtime += delta;
			if(newtime>intime){
				frameTime = basicFrameTime;
				intime = 0;
			}
		}
		
	}
	
	public void setRunFrame(int startFrame, int endFrame, float frametime){
		this.frameTime = frametime;
		this.startFrame = startFrame;
		this.endFrame = endFrame;
		frame = startFrame;
		time = 0;
		intime = 0;
	}
	
	public void setFramesImage(TextureRegion[] frames, int startFrame, int endFrame, float frametime){
		this.frames = frames;
		setRunFrame(startFrame, endFrame, frametime);
		frame = startFrame;
	}
	
	public void setFrameTime(float time){
		frameTime = time;
	}
	
	public boolean isCompleted(){
		return isCompleted;
	}
	
	public boolean isPrepare(){
		return pretime>0;
	}
	
	public int getFrame(){
		if(frame>endFrame)		return startFrame;
		return frame;
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public TextureRegion getFrameImage(){
		return frames[getFrame()];
	}
	
	public float getWidth(){
		return frames[getFrame()].getRegionWidth()*rate;
	}

	public float getHeight(){
		return frames[getFrame()].getRegionHeight()*rate;
	}
	
	public void changeFrameTime(float newframeTime, float intime){
		newtime = 0;
		this.intime = intime;
		frameTime = newframeTime;
	}
}
