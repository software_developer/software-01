package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GunPower {
	TextureRegion image;
	TextureRegion headImg;
	int kind;
	int state;
	int cost;
	int damage;
	int dameUps;
	int speedUps;
	int bonusDame;
	float bonusSpeed;
	float speed;
	float originX;
	float originY;
	float headtogun;
	
	public GunPower(TextureRegion image, TextureRegion headImg, int kind, int state, int cost, int damage, 
			float speed, int dameUps, int bonusDame, int speedUps, float bonusSpeed, float headtogun, float oriX, float oriY){
		this.image = image;
		this.headImg = headImg;
		this.kind = kind;
		this.state = state;
		this.cost = cost;
		this.damage = damage;
		this.speed = speed;
		this.dameUps = dameUps;
		this.speedUps = speedUps;
		this.bonusDame = bonusDame;
		this.bonusSpeed = bonusSpeed;
		this.headtogun = headtogun;
		this.originX = oriX;
		this.originY = oriY;
	}
	
	public GunPower(Texture image, Texture headImg, int kind, int state, int cost, int damage, float speed, 
			int dameUps, int bonusDame, int speedUps, float bonusSpeed, float headtogun, float oriX, float oriY){
		this.image = new TextureRegion(image);
		this.headImg = new TextureRegion(headImg);
		this.kind = kind;
		this.state = state;
		this.cost = cost;
		this.damage = damage;
		this.speed = speed;
		this.dameUps = dameUps;
		this.speedUps = speedUps;
		this.bonusDame = bonusDame;
		this.bonusSpeed = bonusSpeed;
		this.headtogun = headtogun;
		this.originX = oriX;
		this.originY = oriY;
	}
	
	public void upDamage(){
		dameUps ++;
	}
	
	public void upSpeed(){
		speedUps ++;
	}
	
	public void setState(int state){
		this.state = state;
	}
	
	public int getState(){
		return state;
	}
	
	public int getDamage(){
		return damage + bonusDame*dameUps;
	}
	
	public float getSpeed(){
		return speed + bonusSpeed*speedUps;
	}
	
	public int getKind(){
		return kind;
	}
	
	public boolean isLocked(){
		return state>1;
	}

	public int getDameUps() {
		return dameUps;
	}

	public void setDameUps(int dameUps) {
		this.dameUps = dameUps;
	}

	public int getSpeedUps() {
		return speedUps;
	}

	public void setSpeedUps(int speedUps) {
		this.speedUps = speedUps;
	}
	
	
}
