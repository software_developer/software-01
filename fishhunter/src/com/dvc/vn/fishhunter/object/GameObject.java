package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class GameObject {
	protected TextureRegion image;
	protected float x;
	protected float y;
	protected Vector2 origin;
	protected float rotation;
	
	public GameObject(TextureRegion image, float x, float y, float rotation) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.rotation = rotation;
	}
	
	public GameObject(TextureRegion image, float x, float y) {
		this.image = image;
		this.x = x;
		this.y = y;
		this.rotation = 0;
	}
	
	public Vector2 getOrigin() {
		return origin;
	}
	
	public float getOriginPosX(){
		return x-origin.x;
	}
	
	public float getOriginPosY(){
		return y-origin.y;
	}

	public void setOrigin(Vector2 origin) {
		this.origin = origin;
	}

	public GameObject(Texture image, float x, float y, float rotation, float originX, float originY) {
		this.image = new TextureRegion(image);
		this.x = x;
		this.y = y;
		this.rotation = rotation;
		this.origin = new Vector2(originX, originY);
	}
	
	public GameObject(TextureRegion image, float x, float y, float rotation, float originX, float originY) {
		this.image = new TextureRegion(image);
		this.x = x;
		this.y = y;
		this.rotation = rotation;
		this.origin = new Vector2(originX, originY);
	}
	
	public GameObject(Texture image, float x, float y, float rotation) {
		this.image = new TextureRegion(image);
		this.x = x;
		this.y = y;
		this.rotation = rotation;
	}
	
	public GameObject(Texture image, float x, float y) {
		this.image = new TextureRegion(image);
		this.x = x;
		this.y = y;
		this.rotation = 0;
	}

	public TextureRegion getImage() {
		return image;
	}

	public void setImage(TextureRegion image) {
		this.image = image;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getRotation() {
		return rotation;
	}

	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	public float getWidth(){
		return image.getRegionWidth();
	}
	
	public float getHeight(){
		return image.getRegionHeight();
	}
}
