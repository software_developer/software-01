package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class CoinStage {
	public TextureRegion image;
	public float x;
	public float y;
	public float vy;
	public float aColor;
	public float time;
	public float basicTime;
	public int numb;
	
	public CoinStage(float x, float y, int numb) {
		this.x = x;
		this.y = y;
		this.vy = 30;
		this.aColor = 0;
		this.time = 0;
		this.numb = numb;
		this.basicTime = .3f;
	}
	
	public CoinStage(TextureRegion image, float x, float y, float vy, float basicTime) {
		this.image = new TextureRegion(image);
		this.x = x;
		this.y = y;
		this.vy = vy;
		this.aColor = 0;
		this.time = 0;
		this.basicTime = basicTime;
	}
	
	public CoinStage(float x, float y, int numb, float vy) {
		super();
		this.x = x;
		this.y = y;
		this.vy = vy;
		this.aColor = 0;
		this.time = 0;
		this.numb = numb;
		this.basicTime = .3f;
	}
	
	public void update(float delta){
		time += delta;
		y += vy*delta/(basicTime*3);
		if(time<basicTime)	aColor += delta/basicTime;
		if(time>(basicTime*2))	aColor -= delta/basicTime;
	}
	
	public float getColor(){
		if(aColor>1)	return 1;
		if(aColor<0)	return 0;
		return aColor;
	}
	
	public void upTheTime(){
		aColor = 0;
	}
	
	public boolean isTimeUp(){
		return aColor<=0;
	}
	
	public void reset(){
		time = 0;
		aColor = 1;
	}
	
	public void setPos(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	public TextureRegion getImage(){
		return image;
	}
}
