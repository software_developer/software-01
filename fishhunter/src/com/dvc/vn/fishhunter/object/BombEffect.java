package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class BombEffect {
	private TextureRegion[] image;
	private int frame;
	private float x, y;
	private float time, frametime;
	
	public BombEffect(TextureRegion[] frames, float x, float y, float frameTime){
		image = frames;
		this.x = x;
		this.y = y;
		frame = 0;
		time = 0;
		frametime = frameTime;
	}
	
	public void update(float delta){
		time += delta;
		if(time>frametime){
			frame ++;
			time = 0;
		}
	}
	
	public TextureRegion getImage(){
		if(frame>=image.length)	return image[image.length-1];
		return image[frame];
	}
	
	public float getX(){
		return x;
	}
	
	public float getY(){
		return y;
	}
	
	public int getWidth(){
		return image[frame].getRegionWidth();
	}
	
	public int getHeight(){
		return image[frame].getRegionHeight();
	}
	
	public boolean isCompleted(){
		return frame>=image.length;
	}
}
