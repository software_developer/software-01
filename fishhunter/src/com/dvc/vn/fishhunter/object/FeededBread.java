package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.math.Rectangle;

public class FeededBread {
	private Rectangle rec;
	private float rota, rate;
	private float time, life, lifetime;
	
	public FeededBread(int x, int y, int width, int height, float lifetime){
		rec = new Rectangle(x-width/2, y-height/2, width, height);
		this.lifetime = lifetime;
		rota = 0;
		rate = 1;
		time = 0;
		life = 0;
	}
	
	public void update(float delta){
		time += delta;
		life += delta;
		if(time>.4f){
			if(rota==0){
				rota = -5;
				rate = 1.2f;
			}
			else{
				rota = 0;
				rate = 1;
			}
			time = 0;
		}
	}
	
	public float getX() {
		return rec.x+rec.width/2;
	}

	public float getY() {
		return rec.y+rec.height/2;
	}
	
	public float getRegionWidth(){
		return rec.width;
	}
	
	public float getRegionHeight(){
		return rec.height;
	}
	
	public Rectangle getRectangle(){
		return rec;
	}

	public float getRota() {
		return rota;
	}
	
	public float getRate() {
		return rate;
	}

	public boolean isCompleted(){
		return life>lifetime;
	}
}
