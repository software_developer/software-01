package com.dvc.vn.fishhunter.object;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class FishInfo {
	
	int hp;
	int kind;
	int coin;
	int dyingStartFrame;
	int dyingEndFrame;
	public float appear;
	float dyingFrameTime;
	float dyingTime;
	float speed;
	float boostSpeed;
	float basicSpeed;
	float originX;
	float originY;
	float aniBoostFrameTime;
	float boostTime;
	AnimationObject animation;
	
	public FishInfo(TextureRegion[] t, float originX, float originY, float speed, float boostSpeed, float boostTime, int kind, int hp, int coin, float appear,
			int startFrame, int endFrame, float aniFrameTime, float aniBoostFrameTime, int dyingst, int dyingend, float dyingfrtime, float dyingtime) {
		this.originX = originX;
		this.originY = originY;
		this.basicSpeed = speed;
		this.speed = speed;
		this.boostSpeed = boostSpeed;
		this.boostTime = boostTime;
		this.kind = kind;
		this.hp = hp;
		this.coin = coin;
		this.appear = appear;
		this.aniBoostFrameTime = aniBoostFrameTime;
		this.dyingStartFrame = dyingst;
		this.dyingEndFrame = dyingend;
		this.dyingFrameTime = dyingfrtime;
		this.dyingTime = dyingtime;
		animation = new AnimationObject(t, aniFrameTime, 0, startFrame, endFrame);
	}
}
