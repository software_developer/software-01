package com.dvc.vn.fishhunter.object;

import java.util.Random;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class TwinkyStar extends GameObject{

	private float scale;
	private float growtime;
	private float destroytime;
	private Vector2 veloc;
	
	public TwinkyStar(TextureRegion image, float x, float y, float rotation, float originX, float originY) {
		super(image, x, y, rotation, originX, originY);
	}
	
	public void randomParam(Random rand){
		rotation = rand.nextInt(90);
		scale = rand.nextFloat()*.5f;
		growtime = .8f;
		destroytime = .8f;
		veloc = new Vector2(rand.nextInt(100)-30, rand.nextInt(100)-30);
	}
	
	public void update(float delta){
		if(growtime>0){
			growtime -= delta;
			if(scale<1)		scale += delta/2;
			else	scale = 1;
		}
		else if(destroytime>0){
			destroytime -= delta;
			if(scale>0)	scale -= delta;
			else	scale = 0;
		}
		rotation += 50*delta;
		x += veloc.x*delta;
		y += veloc.y*delta;
	}
	
	public boolean isDestroyed(){
		return destroytime<=0;
	}
	
	public float getScale(){
		return scale;
	}
}
