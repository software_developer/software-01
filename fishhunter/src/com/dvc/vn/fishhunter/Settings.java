package com.dvc.vn.fishhunter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


import com.badlogic.gdx.Gdx;
import com.dvc.vn.fishhunter.ultility.SaveGame;

public class Settings {
	public static boolean delmsg;
	public static boolean isFirstTime;
	public static boolean alreadyLoad;
	public static boolean ispause;
	public static boolean soundEnabled = true;
	public static int level;
	public static int exp;
	public static int requiredExp;
	public static int power;
	public static int coin;
	public static int shell;
	public static int bombNumb;
	public static int breadNumb;
	public static int shockNumb;
	public static int[] fishs;

	public static String file = ".raz.fishhunter";
	public static String pack = "raz.fishhunter.1";
	public static String msg1 = "", msg2 = "";
	
	public static final int EXIT = 0;
	public static final int SMS = 1;
	public static final int SHARE_FB = 2;
	public static final int SHOW_MSG = 3;
	
	public static void load () {
		soundEnabled = SaveGame.getBoolean("soundEnabled", true);
		level = SaveGame.getInt("level", 1);
		exp = SaveGame.getInt("exp", 0);
		requiredExp = SaveGame.getInt("requiredExp", 200);
		power = SaveGame.getInt("power", 0);
		coin = SaveGame.getInt("coin", 1000);
		shell = SaveGame.getInt("shell", 100);
		bombNumb = SaveGame.getInt("bombNumb", 3);
		breadNumb = SaveGame.getInt("breadNumb", 3);
		shockNumb = SaveGame.getInt("shockNumb", 3);
		fishs = new int[13];
		for (int i = 0; i < 13; i++) {
			fishs[i] = SaveGame.getInt("fishs"+i, 0);
		}
	}

	public static void save () {
		SaveGame.putBoolean("soundEnabled", soundEnabled);
		SaveGame.putInt("level", level);
		SaveGame.putInt("exp", exp);
		SaveGame.putInt("requiredExp", requiredExp);
		SaveGame.putInt("power", power);
		SaveGame.putInt("coin", coin);
		SaveGame.putInt("shell", shell);
		SaveGame.putInt("bombNumb", bombNumb);
		SaveGame.putInt("breadNumb", breadNumb);
		SaveGame.putInt("shockNumb", shockNumb);
		for (int i = 0; i < 13; i++) {
			SaveGame.putInt("fishs"+i, fishs[i]);
		}
		for(int i=0;i<9;i++){
			SaveGame.putInt("state"+i, GunManager.gun[i].getState());
			SaveGame.putInt("dameups"+i, GunManager.gun[i].getDameUps());
			SaveGame.putInt("speedups"+i, GunManager.gun[i].getSpeedUps());
		}
		
	}
}
