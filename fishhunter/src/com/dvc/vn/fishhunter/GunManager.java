package com.dvc.vn.fishhunter;

import com.dvc.vn.fishhunter.object.GunPower;

public class GunManager {
	public static GunPower[] gun = {
		//Gunimage - HeadImage - kind - state - cost - damage - speed - dameUps - bonusDame - speedUps - bonusSpeed - HeadToGunRate - originX - originY
		new GunPower(Assets.gun[0], Assets.gunhead[0], 1, 1, 1, 20, .7f, 0, 5, 0, .07f, .61f, 49, 30),
		new GunPower(Assets.gun[1], Assets.gunhead[1], 2, 1, 2, 40, .77f, 0, 10, 0, .077f, .61f, 49, 30),
		new GunPower(Assets.gun[2], Assets.gunhead[2], 3, 1, 3, 60, .84f, 0, 15, 0, .084f, .61f, 49, 30),
		new GunPower(Assets.gun[3], Assets.gunhead[3], 4, 1, 4, 100, .91f, 0, 20, 0, .091f, .61f, 49, 30),
		new GunPower(Assets.gun[4], Assets.gunhead[4], 5, 1, 5, 120, .98f, 0, 25, 0, .098f, .61f, 49, 30),
		new GunPower(Assets.gun[5], Assets.gunhead[5], 6, 1, 6, 150, 1.05f, 0, 30, 0, .105f, .61f, 49, 30),
		new GunPower(Assets.gun[6], Assets.gunhead[6], 7, 1, 7, 180, 1.12f, 0, 35, 0, .112f, .61f, 49, 30),
		new GunPower(Assets.gun[7], Assets.gunhead[7], 8, 2, 8, 200, 1.19f, 0, 40, 0, .119f, .61f, 49, 30),
		new GunPower(Assets.gun[8], Assets.gunhead[8], 9, 3, 9, 250, 1.26f, 0, 45, 0, .126f, .61f, 49, 30)
	};
}
