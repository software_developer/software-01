package com.dvc.vn.fishhunter;

import java.util.Random;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.dvc.vn.fishhunter.object.BulletObject;
import com.dvc.vn.fishhunter.object.TrunkObject;
import com.dvc.vn.fishhunter.ultility.OverlapTester;

public class TestScreen implements Screen, InputProcessor{

	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	private int CONS_TIME_GLANCE = 5;
	private int stateButtonBonus;
	private Rectangle buttonBonusBound;
	private boolean isRotate;
	private float timeRotate;
	private float rotation;
	private float velocity;
	private int stateBonus;
	private int kindBonus; // loại phần thưởng, 1-quay vòng, 2 là chọn hòm, thay đổi giá trị trong hàm show
	private int count;
	private float timeMoving;
	private float timeDrawBonus;
	private float timeGlance;
	private BulletObject coinBonus;
	private boolean drawEye;
	private TrunkObject trunk[];
	private int a,b,c;
	private boolean creatCoinBonus;
	
	public TestScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
		buttonBonusBound = new Rectangle(325, 55, 149, 44);
		kindBonus = 2;
	}
	
	public void setKindBonus(int k){
		kindBonus = k;
	}
	
	private void drawBonus(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(1, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		batcher.draw(Assets.scene[Settings.level%2], 0, 0);
		batcher.draw(Assets.content, 0, 0);
		batcher.draw(Assets.table, 346, 0);
		batcher.draw(Assets.contentCoin, 10, 3);
		batcher.draw(Assets.next, 455, -5, 54, 54);
		batcher.draw(Assets.prev, 290, -5, 54, 54);
		Assets.fontSmallMoney.draw(batcher, game.gamescreen.formatNumber((int)game.gamescreen.coinDraw, 9), 45, 30);
		Assets.fontCountTime.draw(batcher, ""+(int)game.gamescreen.timeAddCoin, 172, 30);
		batcher.draw(Assets.contentPower, 565, 3);
		batcher.draw(Assets.power ,571, 8, 98*Settings.power/100, 14, 0, 0, (int)(98*Settings.power/100), 14, false, false);
		batcher.draw(Assets.contentShell, 685, 3);
		Assets.fontSmallMoney.draw(batcher, game.gamescreen.formatNumber(Settings.shell, 6), 717, 27);
		if(kindBonus == 1){
			batcher.draw(Assets.contentShop, 74, 28);
			batcher.draw(Assets.fishCord, 105, 50);
			batcher.draw(Assets.prizes, 300, 380);
			batcher.draw(Assets.listbonus, 260, 120);
			switch (stateButtonBonus) {
			case 1:
				batcher.draw(Assets.buttonSpin, 325, 55);
				break;
			case 2:
				batcher.draw(Assets.buttonSpinPrs, 325, 55);
				break;
			case 3:
				batcher.draw(Assets.buttonStopDisable, 325, 55);
				break;
			case 4:
				batcher.draw(Assets.buttonStop, 325, 55);
				break;
			case 5:
				batcher.draw(Assets.buttonStopPrs, 325, 55);
				break;
			case 6:
				batcher.draw(Assets.buttonStopDisable, 325, 55);
				break;
			default:
				break;
			}
			batcher.draw(Assets.round, 408, 95, 285/2, 285/2, 285, 285, 1, 1, rotation);
			batcher.draw(Assets.circle, 405, 95);
		} else if(kindBonus == 2){
			batcher.draw(Assets.contentShop, 74, 28);
			batcher.draw(Assets.decoration, 85, 275);
			batcher.draw(Assets.treasure, 220, 360);
			batcher.draw(Assets.nami, 550, 20);
			if(drawEye)
				batcher.draw(Assets.eyeNami, 627, 407);
			for(int i = 0; i < trunk.length; i++){
				if(trunk[i].getState() == 5){
					batcher.draw(Assets.effectTrunk, 150,50);
				}
				batcher.setColor(1, 1, 1, trunk[i].getaColor());
				batcher.draw(trunk[i].getImage(), trunk[i].getX(), trunk[i].getY(), trunk[i].getWidth(), trunk[i].getHeight());
				batcher.setColor(1, 1, 1, 1);
			}
		}
		if(coinBonus != null){
			batcher.setColor(1, 1, 1, coinBonus.getAcolor());
			batcher.draw(coinBonus.getImage(), coinBonus.getX()-coinBonus.getOrigin().x, coinBonus.getY()-
					coinBonus.getOrigin().y, coinBonus.getOrigin().x, coinBonus.getOrigin().y, 
					coinBonus.getWidth(), coinBonus.getHeight(), 1, 1, coinBonus.getRotation());
		}
		batcher.setColor(1, 1, 1, 1);

		batcher.end();
	}
	
	private void updateBonus(float delta){
		if(coinBonus != null){
			if(coinBonus.reachedTarget()){
				coinBonus.setImage(Assets.coinsbonnus[stateBonus]);
				coinBonus.explose();
			}
			coinBonus.update(delta);
			if(coinBonus.isDestroyed()){
				switch (stateBonus) {
				case 0:
					Settings.coin += 100;
					break;
				case 1:
					Settings.coin += 150;
					break;
				case 2:
					Settings.coin += 180;
					break;
				case 3:
					Settings.coin += 300;
					break;
				case 4:
					Settings.coin += 400;
					break;
				case 5:
					Settings.coin += 500;
					break;
				case 6:
					Settings.coin += 200;
					break;
				case 7:
					Settings.coin += 600;
					break;
				default:
					break;
				}
				coinBonus = null;
				kindBonus = 0;
				game.setScreen(game.gamescreen);
			}

		}
		if(kindBonus == 1){
			if(isRotate){
				timeRotate += delta;
				if(timeRotate >= 3){
					timeRotate = 0;
					stateButtonBonus = 4;
				}
				if(velocity >= -15) {
					velocity -= delta*5;
				}
				rotation += velocity;
			}
			else{
				if(velocity < 0){
					velocity += 3*delta;
					rotation += velocity;
				}
				else{
					velocity = 0;
					if(stateButtonBonus == 3){
						while(rotation < -360){
							rotation += 360;
						}
						if(timeDrawBonus < 1){
							timeDrawBonus += delta;
						}
						else{
							if(rotation >= -12){
								stateBonus = 5;
							} else if(rotation >= -113){
								stateBonus = 1;
							} else if(rotation >= -134){
								stateBonus = 4;
							} else if(rotation >= -220){
								stateBonus = 2;
							} else if(rotation >= -255){
								stateBonus = 3;
							} else stateBonus = 0;
							coinBonus =  new BulletObject(Assets.coinsbonnus[stateBonus], 350, 250, 
									0, 157, 55, new Vector2(350, 350), 1);
							coinBonus.fixVelocity(200);
							coinBonus.setSpeedScale(0.5f);
							stateButtonBonus = 6;
							timeDrawBonus = 0;
							Assets.playSound(Assets.sound_treasureGain);
						}
					}
				}
			}
		} else if(kindBonus == 2){
			timeGlance += delta;
			if(timeGlance > CONS_TIME_GLANCE && timeGlance < CONS_TIME_GLANCE + .2){
				drawEye = true;
			}
			else{
				drawEye = false;
			}
			if(timeGlance >= CONS_TIME_GLANCE + .2) timeGlance = 0;
			if(trunk[0].getState() == 2)
				timeMoving += delta;
			if(timeMoving >= 0.2 && count <= 12){
				timeMoving = 0;
				if(count > 0){
					TrunkObject t = trunk[a];
					trunk[a] = trunk[b];
					trunk[b] = t;
				}
				count++;
				a = random(4)-1;
				b = random(4)-1;
				while(b == a){
					b = random(4)-1;
				}
				Vector2 v1 = trunk[a].getSlot(a), v2 = trunk[b].getSlot(b);
				trunk[a].move(v2);
				trunk[b].move(v1);
				
			}
			for(int i = 0; i < trunk.length; i++){
				trunk[i].update(delta);
				if(trunk[i].getState() == 5 && !creatCoinBonus){
					if(trunk[i].getCoin() == 200) stateBonus = 6;
					else stateBonus = 7;
					coinBonus =  new BulletObject(Assets.coinsbonnus[stateBonus], 350, 200, 
							0, 157, 55, new Vector2(350, 350), 1);
					coinBonus.fixVelocity(200);
					coinBonus.setSpeedScale(0.5f);
					creatCoinBonus = true;
				}
			}
		}
	}
	
	public void setStateTrunk(){
		for(int i = 0; i < trunk.length; i++){
			if(i != c) trunk[i].setState(3);
			else {
				trunk[i].setState(4);
				trunk[i].move(new Vector2(267, 132));
			}
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos = new Vector3();
		guiCam.unproject(touchpos.set(Gdx.input.getX(),Gdx.input.getY(), 0));
		if (OverlapTester.pointInRectangle(buttonBonusBound, touchpos.x ,touchpos.y) && kindBonus == 1) {
			Assets.playSound(Assets.sound_button);
			if(stateButtonBonus == 1){
				stateButtonBonus = 2;
			} else if(stateButtonBonus == 4){
				stateButtonBonus = 5;
			}
		}
		for(int i = 0; i < trunk.length;i++){
			if (OverlapTester.pointInRectangle(trunk[i].getRec(), touchpos.x ,touchpos.y) && kindBonus == 2 && count == 13) {
				c = i;
				count++;
				setStateTrunk();
				Assets.playSound(Assets.sound_treasureGain);
			}
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(stateButtonBonus == 2) {
			stateButtonBonus = 3;
			isRotate = true;
		} else if(stateButtonBonus == 5) {
			stateButtonBonus = 3;
			isRotate = false;
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void render(float delta) {
		updateBonus(delta);
		drawBonus(delta);
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(game.testscreen);
		stateButtonBonus = 1;
		isRotate = false;
		timeRotate = 0;
		rotation = 0;
		velocity = 0;
		timeDrawBonus = 0;
		c = -1;
		timeMoving = 0;
		count = 0;
		timeGlance = 0;
		drawEye = false;
		creatCoinBonus = false;
		trunk = new TrunkObject[4];
		trunk[0] = new TrunkObject(Assets.trunkBigGold, 1, 1, 600,0);
		trunk[1] = new TrunkObject(Assets.trunkGold, 1, 1, 200,1);
		trunk[2] = new TrunkObject(Assets.trunkGold, 1, 1, 200,2);
		trunk[3] = new TrunkObject(Assets.trunkGold, 1, 1, 200,3);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
	
	public int random(int numb) {
		Random r = new Random();
		int a = r.nextInt(numb) + 1;
		return a;
	}

}
