package com.dvc.vn.fishhunter;

import com.dvc.vn.fishhunter.object.FishInfo;

public class FishManager {
	
	public static int KINDS = 11;
	public static FishInfo[] smalls = {
		//image - originX - originY - speed - boostSpeed - boostTime - kind - health - coin - appear - startFrame - endFrame - aniFrameTime - aniBoostFrameTime - dyingStartFrame - dyingEndFrame - dyingFrameTime - dyingTime
		new FishInfo(Assets.smallFish[0], 40, 13, .5f, .75f, 1, 0, 30, 1, .002f, 0, 11, .05f, .03f, 12, 17, .1f, 1),
		new FishInfo(Assets.smallFish[1], 46, 20, .4f, .6f, 1, 1, 50, 4, .002f, 0, 11, .06f, .04f, 12, 17, .1f, 1),
		new FishInfo(Assets.smallFish[2], 52, 20, .3f, .45f, 1, 2, 100, 7, .002f, 0, 11, .07f, .04f, 12, 17, .1f, 1),
		new FishInfo(Assets.smallFish[3], 46, 18, .2f, .3f, 1, 3, 200, 10, .002f, 0, 11, .08f, .05f, 12, 17, .1f, 1),
		new FishInfo(Assets.smallFish[4], 38, 25, .1f, .15f, 1, 4, 300, 30, .001f, 0, 11, .09f, .06f, 12, 17, .1f, 1)
	};
	
	public static FishInfo[] bigs = {
		new FishInfo(Assets.bigFish[0], 80, 32, .2f, .3f, 1, 5, 500, 15, .001f, 0, 11, .08f, .06f, 12, 17, .1f, 1),
		new FishInfo(Assets.bigFish[1], 69, 42, .2f, .3f, 1, 6, 650, 20, .001f, 0, 11, .08f, .06f, 12, 17, .1f, 1),
		new FishInfo(Assets.bigFish[2], 50, 73, .16f, .24f, 1, 7, 1000, 40, .001f, 0, 11, .15f, .1f, 12, 17, .1f, 1),
		new FishInfo(Assets.bigFish[3], 96, 28, .1f, .15f, 1, 8, 1250, 50, .001f, 0, 11, .15f, .1f, 12, 17, .1f, 1),
		new FishInfo(Assets.bigFish[4], 147, 70, .18f, .27f, 1, 9, 1500, 60, .001f, 0, 11, .08f, .06f, 12, 17, .1f, 1),
		new FishInfo(Assets.bigFish[5], 140, 56, .25f, .37f, 1, 10, 2000, 80, .001f, 0, 11, .08f, .06f, 12, 17, .1f, 1)
	};
	public static FishInfo shark = new FishInfo(Assets.shark, 144, 52, .25f, .37f, 1, 11, 4000, 0, .00005f, 0, 11, .08f, .06f, 12, 17, .1f, 1);
	public static FishInfo fairy = new FishInfo(Assets.fairy, 115, 60, .3f, .45f, 1, 12, 5000, 0, .00005f, 0, 14, .08f, .06f, 15, 20, .1f, 1);
}
