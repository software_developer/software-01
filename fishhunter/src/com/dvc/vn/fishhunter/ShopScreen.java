package com.dvc.vn.fishhunter;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.dvc.vn.fishhunter.object.GunShopObject;
import com.dvc.vn.fishhunter.ultility.OverlapTester;

public class ShopScreen implements Screen, InputProcessor{
	
	MainGame game;
	OrthographicCamera guiCam;
	SpriteBatch batcher;
	Vector3 touchpos;
	private int kindShop;
	private int selectGun;
	private int selectItem;
	private GunShopObject[] gunList;
	private String s1 = "Your current coins";
	private String s2 = "Power:    ";
	private String s3 = "Upgrade";
	private String s4 = "Speed:    ";
	private String s5 = "Max Power";
	private String s6 = "Max Speed";
	private String s7 = "Unlock";
	private String s8 = "Your current shells";
	private String s9 = "Current count: ";
	private String s10 = "Buy";
	private int cost[] = {8, 20, 8, 20};
	private String function[] = {"Place the bait to attract fishes nearby.",
			"Explode the bomb to catch fishes!","Freeze and stun fishes.","Add 500 coins."};
	private Rectangle[] gunChoiceBound;
	private Rectangle[] itemChoiceBound;
	private Rectangle content1Bound,content2Bound;
	private Rectangle contentBuy;
	private boolean selectcontent1,selectcontent2;
	private Rectangle choiceCannonBound,choiceItemBound;
	private Rectangle addCoinBound;
	private Rectangle btn_back;
	private boolean exit, fromGame;
	private boolean buyCoin, buyItem;
	
	public ShopScreen(MainGame game){
		this.game = game;
		guiCam = new OrthographicCamera(800, 480);
		guiCam.position.set(800 / 2, 480 / 2, 0);
		batcher = new SpriteBatch();
		gunChoiceBound = new Rectangle[9];
		content1Bound = new Rectangle(435, 200, 184, 36);
		content2Bound = new Rectangle(435, 120, 184, 36);
		choiceCannonBound = new Rectangle(135, 427, 125, 22);
		choiceItemBound = new Rectangle(264, 427, 125, 22);
		addCoinBound = new Rectangle(300, 60, 40, 40);
		contentBuy = new Rectangle(430, 100, 199, 45);
		btn_back = new Rectangle(665, 375, 35, 34);
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++){
				gunChoiceBound[i+j*3] = new Rectangle(125+i*90, 330-j*80,77,55);
			}
		choiceItemBound = new Rectangle(264, 427, 125, 22);
		itemChoiceBound = new Rectangle[4];
		for(int i = 0; i < 4; i++){
			itemChoiceBound[i] = new Rectangle(130, 340-i*70,260,50);
		}
	}
	
	public void fromGame(){
		fromGame = true;
	}
	
	public void notfromGame(){
		fromGame = false;
	}
	
	public boolean isFromGame(){
		return fromGame;
	}

	public void drawShop(float delta){
		GLCommon gl = Gdx.gl;
		gl.glClearColor(1, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		guiCam.update();
		batcher.setProjectionMatrix(guiCam.combined);
		batcher.enableBlending();
		batcher.begin();
		batcher.draw(Assets.scene[Settings.level%2], 0, 0);
		batcher.draw(Assets.content, 0, 0);
		batcher.draw(Assets.table, 346, 0);
		batcher.draw(Assets.contentCoin, 10, 3);
		batcher.draw(Assets.next, 455, -5, 54, 54);
		batcher.draw(Assets.prev, 290, -5, 54, 54);
		Assets.fontSmallMoney.draw(batcher, game.gamescreen.formatNumber(Settings.coin, 9), 45, 30);
		Assets.fontCountTime.draw(batcher, ""+(int)game.gamescreen.timeAddCoin, 172, 30);
		batcher.draw(Assets.contentPower, 565, 3);
		batcher.draw(Assets.power ,571, 8, 98*Settings.power/100, 14, 0, 0, (int)(98*Settings.power/100), 14, false, false);
		batcher.draw(Assets.contentShell, 685, 3);
		Assets.fontSmallMoney.draw(batcher, game.gamescreen.formatNumber(Settings.shell, 6), 717, 27);
		batcher.draw(Assets.contentShop, 74, 28);
		if(kindShop == 1){
			batcher.draw(Assets.shopChoice1, 130, 415);
			for(int i = 0; i < 3; i++)
				for(int j = 0; j < 3; j++){
					if(selectGun == i+j*3){
						batcher.draw(Assets.contentShop1Choice, 122+i*90, 327-j*80);
					}
					if(gunList[i+j*3].getState() == 1){
						batcher.draw(Assets.itemGun[i+j*3], 125+i*90, 330-j*80);
					} else if(gunList[i+j*3].getState() == 2){
						batcher.draw(Assets.itemGun[i+j*3], 125+i*90, 330-j*80);
						batcher.draw(Assets.lock, 175+i*90, 320-j*80);
					} else if(gunList[i+j*3].getState() == 3){
						batcher.draw(Assets.itemGun[9], 125+i*90, 330-j*80);
					}
				}

			batcher.draw(Assets.contentShopInfo, 400, 50);
			Assets.fontSimple.draw(batcher, s1, 125, 115);
			batcher.draw(Assets.iconCoin, 125, 60,30,30);
			Assets.fontSimple.draw(batcher, ""+Settings.coin, 165, 85);
			
			batcher.draw(Assets.gunhead[selectGun], 550-Assets.gunhead[selectGun].getRegionWidth()/2*0.7f, 315+96*.61f*0.7f, Assets.gunhead[selectGun].getRegionWidth()/2*0.7f, 
					-96*.61f*0.7f, Assets.gunhead[selectGun].getRegionWidth()*0.7f, Assets.gunhead[selectGun].getRegionHeight()*0.7f, 1, 1, 45);
			batcher.draw(Assets.gun[selectGun], 550-49*0.7f, 315-30*0.7f, 49*0.7f, 30*0.7f, 
									96*0.7f, 90*0.7f, 1, 1, 45);
			if(gunList[selectGun].getState() == 1){
				Assets.fontSimple.draw(batcher, s2+gunList[selectGun].getLevelPower()+" / 20", 460, 260);
				if(gunList[selectGun].getLevelPower() < 20){
					if(!selectcontent1)
						batcher.draw(Assets.contentShop1, 435, 200);
					else batcher.draw(Assets.contentShop1Select, 435, 200);
					batcher.draw(Assets.iconCoin, 445, 205,25,25);
					Assets.fontSimple.draw(batcher, ""+gunList[selectGun].getCostPower(), 480, 226);
					Assets.fontSimple.draw(batcher, s3, 540, 226);
				} else {
					batcher.draw(Assets.contentShop1Disable, 435, 200);
					Assets.fontSimple.draw(batcher, s5, 485, 226);
				}
				Assets.fontSimple.draw(batcher, s4+gunList[selectGun].getLevelSpeed()+" / 20", 460, 180);
				if(gunList[selectGun].getLevelSpeed() < 20){
					if(!selectcontent2)
						batcher.draw(Assets.contentShop1, 435, 120);
					else batcher.draw(Assets.contentShop1Select, 435, 120);
					batcher.draw(Assets.iconCoin, 445, 125,25,25);
					Assets.fontSimple.draw(batcher, ""+gunList[selectGun].getCostSpeed(), 480, 146);
					Assets.fontSimple.draw(batcher, s3, 540, 146);
				} else {
					batcher.draw(Assets.contentShop1Disable, 435, 120);
					Assets.fontSimple.draw(batcher, s6, 485, 146);
				}
			} else {
				Assets.fontSimple.draw(batcher, "Cost: ", 460, 265);
				if(!selectcontent1)
					batcher.draw(Assets.contentShop1, 435, 200);
				else batcher.draw(Assets.contentShop1Select, 435, 200);
				Assets.fontSimple.draw(batcher, s7, 495, 226);
				batcher.draw(Assets.iconCoin, 510, 245,25,25);
				Assets.fontSimple.draw(batcher, ""+gunList[selectGun].getCostUnlock(), 550, 265);
			}
		}
		else{
			batcher.draw(Assets.shopChoice2, 130, 415);
			for(int i = 0; i < 4; i++){
				if(selectItem == i)
					batcher.draw(Assets.contentShop2Choice, 127, 337-i*70);
				batcher.draw(Assets.itemMoney[i], 130, 340-i*70);
			}
			Assets.fontSimple.draw(batcher, s8, 125, 115);
			batcher.draw(Assets.iconShell, 125, 60,30,30);
			Assets.fontSimple.draw(batcher, ""+Settings.shell, 165, 85);
			batcher.draw(Assets.iconShellAdd, 300, 60, 40, 40);
			batcher.draw(Assets.contentShopInfo, 400, 50);
			batcher.draw(Assets.item[selectItem], 500, 300,
					Assets.item[selectItem].getRegionWidth()*0.7f,Assets.item[selectItem].getRegionHeight()*0.7f);
			Assets.fontSimple.drawWrapped(batcher, function[selectItem], 430, 280, 200,HAlignment.CENTER);
			if(selectItem!=3){
				Assets.fontSimple.draw(batcher, s9, 440, 180);
				//số lượng vật phẩm
				switch(selectItem){
					case 0: Assets.fontSimple.draw(batcher, Settings.breadNumb+"", 560, 180);		break;
					case 1: Assets.fontSimple.draw(batcher, Settings.bombNumb+"", 560, 180);		break;
					case 2: Assets.fontSimple.draw(batcher, Settings.shockNumb+"", 560, 180);		break;
				}
			} else{
				Assets.fontSimple.draw(batcher, "Your coins: "+Settings.coin, 440, 180);
			}
			if(!buyItem)
				batcher.draw(Assets.contentShop2, 430, 100);
			else batcher.draw(Assets.contentShop2Select, 430, 100);
			batcher.draw(Assets.iconShell, 440, 105, 30, 30);
			Assets.fontSimple.draw(batcher, ""+cost[selectItem], 480, 130);
			Assets.fontSimple.draw(batcher, s10, 570, 130);
		}
		if(!exit) batcher.draw(Assets.btExit, 665, 375);
		else batcher.draw(Assets.btExitPress, 665, 375);
		batcher.end();
	}
	
	public void updateShop(float delta){
		
	}
	
	public void creatGunShop(){
		gunList = new GunShopObject[9];
		for(int i = 0; i < 9; i++){
			gunList[i] = new GunShopObject(i,GunManager.gun[i].getState(),1,1);
		}
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		touchpos = new Vector3();
		guiCam.unproject(touchpos.set(Gdx.input.getX(),Gdx.input.getY(), 0));
		if(OverlapTester.pointInRectangle(btn_back, touchpos.x ,touchpos.y)){
			Assets.playSound(Assets.sound_button);
			exit = true;
		}
		for(int i = 0; i < 9; i++){
			if (OverlapTester.pointInRectangle(gunChoiceBound[i], touchpos.x ,touchpos.y) && gunList[i].getState()<3 && kindShop == 1) {
				Assets.playSound(Assets.sound_button);
				selectGun = i;
			}
		}
		if (OverlapTester.pointInRectangle(content1Bound, touchpos.x ,touchpos.y) && gunList[selectGun].getLevelPower() < 20 && kindShop == 1) {
			Assets.playSound(Assets.sound_button);
			if(gunList[selectGun].getState() == 3){
				
			} else{
				selectcontent1 = true;
			}
		}
		if (OverlapTester.pointInRectangle(content2Bound, touchpos.x ,touchpos.y) && gunList[selectGun].getLevelSpeed() < 20 && kindShop == 1) {
			Assets.playSound(Assets.sound_button);
			if(gunList[selectGun].getState() == 3){
				
			} else{
				selectcontent2 = true;
			}
		}
		if (OverlapTester.pointInRectangle(choiceCannonBound, touchpos.x ,touchpos.y)) {
			Assets.playSound(Assets.sound_button);
			kindShop = 1;
		}
		if (OverlapTester.pointInRectangle(choiceItemBound, touchpos.x ,touchpos.y)) {
			Assets.playSound(Assets.sound_button);
			kindShop = 2;
		}
		if (OverlapTester.pointInRectangle(addCoinBound, touchpos.x ,touchpos.y) && kindShop == 2) {
			Assets.playSound(Assets.sound_button);
			buyCoin = true;
		}
		if (OverlapTester.pointInRectangle(contentBuy, touchpos.x ,touchpos.y) && kindShop == 2) {
			Assets.playSound(Assets.sound_button);
			buyItem = true;
		}
		for(int i = 0; i < 4; i++){
			if (OverlapTester.pointInRectangle(itemChoiceBound[i], touchpos.x ,touchpos.y) && kindShop == 2) {
				Assets.playSound(Assets.sound_button);
				selectItem = i;
			}
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		if(selectcontent1){
			selectcontent1 = false;
			if(gunList[selectGun].getState() == 2){
				if(selectGun == 7){
					if(Settings.coin >= gunList[7].getCostUnlock()){
						Settings.coin -= gunList[7].getCostUnlock();
						//save coin
						GunManager.gun[7].setState(1);
						GunManager.gun[8].setState(2);
						gunList[7].setState(1);
						gunList[8].setState(2);
					} else{
						//thông báo thiếu tiền
					}
				}
				else{
					if(Settings.coin >= gunList[8].getCostUnlock()){
						Settings.coin -= gunList[8].getCostUnlock();
						//save coin
						GunManager.gun[8].setState(1);
						gunList[8].setState(1);
					} else{
						//thông báo thiếu tiền
					}
				}
			}
			else
				{
					if(Settings.coin >= gunList[selectGun].getCostPower()){
						Settings.coin -= gunList[selectGun].getCostPower();
						//save coin
						gunList[selectGun].setLevelPower(gunList[selectGun].getLevelPower()+1);
						GunManager.gun[selectGun].upDamage();
					}
					else{
						//thông báo thiếu tiền
					}
				}
		}
		if(selectcontent2){
			selectcontent2 = false;
			if(Settings.coin >= gunList[selectGun].getCostSpeed()){
				Settings.coin -= gunList[selectGun].getCostSpeed();
				//save coin
				gunList[selectGun].setLevelSpeed(gunList[selectGun].getLevelSpeed()+1);
				GunManager.gun[selectGun].upSpeed();
			}
			else{
				//thông báo thiếu tiền
			}
		}
		if(buyCoin){
			buyCoin = false;
			if(game.icontact!=null)		game.icontact.onAction(Settings.SMS);
			//thanh toán mua xu
		}
		if(buyItem){
			buyItem = false;
			if(Settings.shell > cost[selectItem]){
				//tăng số lượng
				switch(selectItem){
					case 0:	Settings.breadNumb ++;	break;
					case 1:	Settings.bombNumb ++;	break;
					case 2:	Settings.shockNumb ++;	break;
					case 3:	Settings.coin += 500;	break;
				}
				Settings.shell -= cost[selectItem];
				//save shell
			} else{
				//thông báo ko đủ sò
			}
		}
		if(exit){
			exit = false;
			Settings.save();
			if(fromGame){
				game.gamescreen.updateGunFromShopping();
				game.setScreen(game.gamescreen);
			}
			else	game.setScreen(game.menuscreen);
		}
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public void render(float delta) {
		updateShop(delta);
		drawShop(delta);
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(game.shopscreen);
		kindShop = 1;
		selectGun = 0;
		selectItem = 0;
		selectcontent1 = false;
		selectcontent2 = false;
		buyCoin = false;
		buyItem = false;
		exit = false;
		creatGunShop();
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
